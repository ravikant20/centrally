import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  image:any = "assets/images/profile_img.png";
  imageSrc: string | ArrayBuffer |any = "assets/images/profile_img.png";
  files: any;
  first_name:any = "";
  last_name:any = "";
  email:any = "";
  phone:any = "";
  ShowSubmitLoader:boolean = false
  myGroup = new FormGroup({});
  private formData = new FormData();
  constructor(private _fb: FormBuilder,private router: Router, private service: ServiceFileService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    if(!localStorage.getItem("isAuthenticated")){
      this.router.navigate(['/login'])

    }

    this.myGroup = this._fb.group({
			'first_name': ['', [Validators.required]],
			'last_name': ['', [Validators.required]],
			'email': ['', [Validators.required]],
			'phone': ['', [Validators.required, Validators.minLength(10)]]
		
		})


    this.getData()
    window.scrollTo(0, 0);
    $('.full_page_loader_app').show();
		$('.profile-main').css('opacity',0).hide();
  }

  validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
			const control = myGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}


  editProfile(){
    var click = document.getElementById('fileUpload');
    if(click != undefined){
      click.click();
    }
    
  }

  readURL(event:any) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        // console.log(this.formData)
        reader.readAsDataURL(file); 
      } 
  }

  getData(){
    this.service.getUserData().subscribe(
      (response :any) => {
        // console.log(response)
        // this.name = response['data'].name;
        var string = response['data'].name;
        // console.log(string.split(" ") )
        string = string.split(" ");
        // console.log(string[1])
        this.email = response['data'].email;
        this.phone = response['data'].phone;
        this.first_name = string[0];
        this.last_name = string[1];
        this.imageSrc = response['data'].image
       
          $('.full_page_loader_app').hide();
		$('.profile-main').css('opacity',1).show();
     
      },
      err => {
        console.log(err)
      }
    );
  }


  save(e:any){
		this.validateAllFields(this.myGroup);
    setTimeout(() => {


			if ($(e.target).closest('form').find('div').hasClass('alert')) {


			} else {
    this.formData.append("first_name", this.first_name);
    this.formData.append("last_name", this.last_name);
    this.formData.append("phone", this.phone);
    this.ShowSubmitLoader = true;
    this.service.updateUserData(this.formData).subscribe(
      (response :any) => {
        this.ShowSubmitLoader = false;
        localStorage.setItem("image",response['data']['image'])
        localStorage.setItem("name",response['data']['name'])
        var image  = document.getElementById("changeImage");
        if(image != null){
          (<HTMLImageElement>image).src = response['data']['image']
        }
        var name  = document.getElementById("textChange");
        if(name != null){
          name.innerText = response['data']['name']
        }
        
        Swal.fire({
          title: 'Success',
          text: 'Your profile has been updated successfully',
          icon: 'success',
          confirmButtonText: 'OK'
        })
        
      },
      err => {
        this.ShowSubmitLoader = false;
        Swal.fire({
          title: 'Error!',
          text: 'Something went wrong!',
          icon: 'error',
          confirmButtonText: 'OK'
        })
      }
    );
    }
  }, 1000);
  }


  allowAlphabetic(e:any){
    var regex = new RegExp(/^[a-zA-Z]+$/);
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
        return true;
      }
      else
      {
      e.preventDefault();
      return false;
      }
    }

}
