import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  new_password:any = "";
  new_confirm_password:any = "";
  error:any=[];
  formGroup= new FormGroup({});

  constructor(private router: Router,private _fb: FormBuilder, private service: ServiceFileService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    if(!localStorage.getItem("isAuthenticated")){
      this.router.navigate(['/login'])

    }

    this.formGroup = this._fb.group({
     
       'new_confirm_password': ['',[Validators.required,Validators.minLength(6)]],
       'new_password': ['',[Validators.required,Validators.minLength(6)]]
       

    })


  }

  validateAllFields(formGroup: FormGroup){         
    Object.keys(formGroup.controls).forEach(field => {  
        const control = formGroup.get(field);            
        if (control instanceof FormControl) {             
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        
            this.validateAllFields(control);  
        }
    });
  }



  changePassword(){
    this.error = "";
    if (this.formGroup.valid) {
      
    } else {
        this.validateAllFields(this.formGroup); 
        return;
    }
    var data  = {
      new_password:this.new_password,
      new_confirm_password:this.new_confirm_password,
    }    

    this.service.changePassword(data).subscribe(
      (response :any) => {
        if(response['error'] == 1){
          this.error = "Password not matched or is invalid!";
        }else{
          Swal.fire({
            title: 'Success',
            text: 'Your password has been successfully changed',
            icon: 'success',
            confirmButtonText: 'OK'
          })
          this.new_password = ""
          this.new_confirm_password = ""
        }
        

      })
  }
}
