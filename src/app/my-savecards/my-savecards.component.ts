import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ServiceFileService } from 'src/service-file.service';
import Swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';
import * as moment from 'moment';
import Inputmask from "inputmask";
import { environment } from 'src/environments/environment';


@Component({
	selector: 'app-my-savecards',
	templateUrl: './my-savecards.component.html',
	styleUrls: ['./my-savecards.component.css']
})
export class MySavecardsComponent implements OnInit {

	elements: any;
	card: any = [];
	cdate = new Date();
	cardArray: any = []
	norecordfound: any = false;
	totalcardArray: any = [];
	length: any = 10;
	pageSize: any = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	Index: any = 1;
	cvvNumbers: any = "";
	myGroup = new FormGroup({});
	error: boolean = false;
	cardHolderName: any = "";
	cardNumber: any = "";
	expiryDate: any = "";
	expiryDateError: any = "";
	expiryMonth: any = "";
	expiryYear: any = "";
	cvvNumber: any = "";
	STRIPE_PUBLIC_KEY: any = environment.STRIPE_PUBLIC_KEY;
	submitdisable:any = false;

	constructor(private _fb: FormBuilder, private router: Router, private service: ServiceFileService, private route: ActivatedRoute) { }

	ngOnInit(): void {
		if (!localStorage.getItem("isAuthenticated")) {
			this.router.navigate(['/login'])

		}
		$('.full_page_loader_app').show();
		$('.my-savecard-table').css('opacity', 0).hide();


		this.getcardDetails()

		this.myGroup = this._fb.group({
			'cardHolderName': ['', [Validators.required]],
			'cardNumber': ['', [Validators.required, Validators.minLength(17), Validators.maxLength(19)]],
			'expiryDate': ['', [Validators.required, Validators.minLength(7)]],
			'cvvNumber': ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(3)]],
			'cvvNumbers': ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(3)]],
			'securityCode': ['', [Validators.required]],
		})

		this.loadStripe()
		this.scriptAll();
	}

	getcardDetails() {
		this.service.getcardDetails().subscribe(
			(response: any) => {

				this.totalcardArray = response.message;
				this.cardArray = response.message.slice(0, 10);
				if (this.cardArray.length == 0) {
					this.norecordfound = true;
				} else {
					this.norecordfound = false;

				}
				$('.full_page_loader_app').hide();
				$('.my-savecard-table').css('opacity', 1).show();

			},
			(error: any) => {
				console.log(error);
			}
		)
	}

	remove_save_card(event: any, id: any) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			confirmButtonColor: '#738c72',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.isConfirmed) {
				const data = {
					id: id
				}
				this.service.removecardDetails(data).subscribe(
					(response: any) => {

						if (response.message == 1) {
							this.getcardDetails();
						}
					},
					(error: any) => {
						console.log(error);
					}
				)
			}
		});


	}


	formatTime(time: any) {
		return moment(time, 'hh:mm').format('LT');
	}

	OnPageEvent(event: PageEvent) {
		const startIndex = event.pageIndex * event.pageSize;
		let endIndex = startIndex + event.pageSize;
		if (endIndex > this.totalcardArray.length) {
			endIndex = this.totalcardArray.length;
		}
		this.Index = startIndex + 1;
		this.cardArray = this.totalcardArray.slice(startIndex, endIndex);
	}


	addCardDetails(e: any) {

		var $this = this;


		$(e.target).closest('form').find('div.rerror').show();
		$this.validateAllFields(this.myGroup);
		$('.stripe_error').removeClass('alert alert-danger').text('');
		$this.submitdisable = true;

		setTimeout(() => {


			if ($(e.target).closest('form').find('div').hasClass('alert')) {
				$this.submitdisable = false;


			} else {
				$('.full_page_loader_app').show().addClass('pay');;

				(<any>window).Stripe.setPublishableKey($this.STRIPE_PUBLIC_KEY);
				(<any>window).Stripe.createToken({
					number: $this.cardNumber,
					cvc: $this.cvvNumber,
					exp_month: $this.expiryDate.split('/')[0],
					exp_year: $this.expiryDate.split('/')[1],
					name: $this.cardHolderName
				}, stripeResponseHandler);


				function stripeResponseHandler(status: any, response: any) {
					if (response.error) {
						$('.full_page_loader_app').hide().removeClass('pay');
						$this.submitdisable = false;

						$('.stripe_error').addClass('alert alert-danger').text(response.error.message);
					} else {
						$('.stripe_error').removeClass('alert alert-danger').text('');

						// token contains id, last4, and card type
						var token = response['id'];
						var email = response['card']['name'];
						var name = response['card']['name'];
						var card = response['card'];

						var data = {

							stripeToken: token,
							name: $('#textChange').text(),
							email: email,
							c_no_encrypt: window.btoa($this.cardNumber),
							card:card
							// cvvNumber:this.cvvNumber,

						}

						$this.service.addCardDetails(data).subscribe(
							(response: any) => {

								$('.full_page_loader_app').hide().removeClass('pay');

								if (response.status == 1) {
									$('.modal.show .close').trigger('click');
									Swal.fire({
										title: response.message,
										text: "Thank You",
										icon: "success"
									}).then(function () {

										$this.getcardDetails();
										$(window).trigger('click');
										$('#add-payment form').trigger('reset');
										$this.submitdisable = false;

									});
								} else if (response.status == 2) {
									$this.submitdisable = false;

									$('.stripe_error').addClass('alert alert-danger').text(response.message);

								} else {
									$this.submitdisable = false;

									$('.stripe_error').addClass('alert alert-danger').text(response.message);


								}
							},
							(error: any) => {
								$this.submitdisable = false;

								console.log(error);

								$('.full_page_loader_app').hide().removeClass('pay');


							}
						)




					}
				}
			}

		}, 1000);




	}


	loadStripe() {
		const $this = this;

		const cyear = $this.cdate.getFullYear();

		const cmonth = $this.cdate.getUTCMonth();
		if (!window.document.getElementById('stripe-scriptjs')) {
			const script = window.document.createElement('script');
			script.id = "stripe-scriptjs";
			script.type = "text/javascript";
			script.src = "https://js.stripe.com/v2/";

			window.document.body.appendChild(script);
		}




		var expiryMask = function (event: any) {
			var inputChar = String.fromCharCode(event.keyCode);
			var code = event.keyCode;
			var allowedKeys = [8];
			if (allowedKeys.indexOf(code) !== -1) {
				return;
			}

			event.target.value = event.target.value.replace(
				/^([1-9]\/|[2-9])$/g, '0$1/'
			).replace(
				/^(0[1-9]|1[0-2])$/g, '$1/'
			).replace(
				/^([0-1])([3-9])$/g, '0$1/$2'
			).replace(
				/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2'
			).replace(
				/^([0]+)\/|[0]+$/g, '0'
			).replace(
				/[^\d\/]|^[\/]*$/g, ''
			).replace(
				/\/\//g, '/'
			);
		}

		var splitDate = function ($domobj: any, value: any) {
			var regExp = /(1[0-2]|0[1-9]|\d)\/(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)/;
			var matches = regExp.exec(value);
			if (matches) {
				$domobj.siblings('input[name$="expiryMonth"]').val(matches[1]);
				$domobj.siblings('input[name$="expiryYear"]').val(matches[2]);

			}
		}


		$(document).on("focusout", ".card_exp", function () {
			splitDate($(this), $(this).val());
		});


		$(document).on("keydown", ".card_exp", function () {
			let val = this.value;
			if ($(this).val() == "") {
				$this.expiryDateError = false;

			}

		});
		$(document).on("keyup", ".card_exp", function (event) {
			$this.expiryDateError = false;
			expiryMask(event);

			if ($(this).val()) {
				let val = this.value;


				$this.expiryMonth = val.split('/')[0];
				$this.expiryYear = val.split('/')[1];
				if ($this.expiryMonth < 10 && $this.expiryMonth.toString().length < 2) {
					$this.expiryDateError = true;
				}
				if ('' === $this.expiryMonth || isNaN($this.expiryMonth) || $this.expiryMonth < 1 || $this.expiryMonth > 12) {
					$this.expiryDateError = true;
				}


				if ('' === $this.expiryYear || isNaN($this.expiryYear) || $this.expiryYear.length != 4 || $this.expiryYear < cyear) {
					$this.expiryDateError = true;

				}

				if ($this.expiryYear <= cyear) {
					if ($this.expiryMonth <= cmonth) {
						$this.expiryDateError = true;
					}
				}


			}

		});

	}

	validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
		  const control = myGroup.get(field);
		  if (control instanceof FormControl) {
			control.markAsTouched({ onlySelf: true });
		  } else if (control instanceof FormGroup) {
			this.validateAllFields(control);
		  }
		});
	  }


	  allowAlphabetic(e: any) {
		var regex = new RegExp(/^[a-zA-Z\s]+$/);
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
		  return true;
		}
		else {
		  e.preventDefault();
		  return false;
		}
	  }

	  addCard(event: any) {
	this.error = false;

		this.myGroup.reset();
	
	  }

	  scriptAll(){
		Inputmask({placeholder: "",showMaskOnHover: false, // will do nothing, mask will be shown on hover
		showMaskOnFocus: false}).mask(document.querySelectorAll(".inputmask"));
	setInterval(function(){
	
	  Inputmask({placeholder: "",showMaskOnHover: false, // will do nothing, mask will be shown on hover
	  showMaskOnFocus: false}).mask(document.querySelectorAll(".inputmask"));
	},3000);
	  }
}
