import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MySavecardsComponent } from './my-savecards.component';

describe('MySavecardsComponent', () => {
  let component: MySavecardsComponent;
  let fixture: ComponentFixture<MySavecardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MySavecardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MySavecardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
