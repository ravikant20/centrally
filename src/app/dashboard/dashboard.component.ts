import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';
import { flatten } from '@angular/compiler';
import * as moment from 'moment';
import { PageEvent } from '@angular/material/paginator';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	classArray: any = [];
	totalclassbyuserArray: any = [];
	classArraytoday: any = [];
	teachersArray: any = [];
	categoriesArray: any = [];
	classbyuserArray: any = [];
	mindfullexist: any = false;
	musicArray: any = [];
	no_of_repetitions: any = "";
	sessionDatearray: any = [];
	viewclassarray: any = [];
	subcategorylistData: any;
	subArray: any;
	baseurls = environment.adminbaseUrl;
	norecordfound: any = false;
	length: any = 10;
	pageSize: any = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	Index: any = 1;
	constructor(
		private router: Router,
		private service: ServiceFileService
	) { }

	ngOnInit(): void {
		if (!localStorage.getItem("isAuthenticated")) {
			this.router.navigate(['/login'])

		}
		window.scrollTo(0, 0);
		$('.full_page_loader_app').show();
		$('.last-week').css('opacity', 0).hide();
		this.getClassesByuser();
		this.getMusic();
		this.getCategories();
		this.subcatListing();


	}

	getClasses() {
		this.service.getClasses().subscribe(
			(response: any) => {
				this.classArray = response.message;


			},
			err => {
				console.log("err", err)
			}
		);
	}

	getClassesByToday() {
		this.service.getClassesByToday().subscribe(
			(response: any) => {
				this.classArraytoday = response.message;
			},
			err => {
				console.log("err", err)
			}
		)
	}


	getClassesByuser() {
		this.service.getClassesByUser().subscribe(
			(response: any) => {
				this.totalclassbyuserArray = response.message;
				this.classbyuserArray = response.message.slice(0, 10);

				if (this.classbyuserArray.length > 0) {
					var counter: any = 0;
					var amount: any = 0;
					for (var i = 0; i < this.classbyuserArray.length; i++) {
						if (parseInt(this.classbyuserArray[i].categories_id) == 1 && parseInt(this.classbyuserArray[i].amount) > 0) {
							counter += 1;
						} else if (parseInt(this.classbyuserArray[i].categories_id) == 1 && parseInt(this.classbyuserArray[i].amount) == 0) {

						}
					}

					if (counter > 0) {
						this.mindfullexist = true;

					} else {

						this.mindfullexist = false;

					}
					this.norecordfound = false;

				} else {

					this.norecordfound = true;

				}

			},
			err => {
				console.log("err", err)
			}
		)
	}


	getMusic() {
		const $this = this;

		this.service.getMusic().subscribe(
			(response: any) => {
				this.musicArray = response.data
				if (this.musicArray.length == 0) {
					this.norecordfound = true;
				} else {
					this.norecordfound = false;

				}
				setTimeout(function () {
					$('.full_page_loader_app').hide();
					$('.last-week').css('opacity', 1).show();
				}, 1000);
			},
			err => {
				console.log("err", err)
			}
		);

	}

	downloadMusic(fileurl: any, filename: any) {
		const data = {
			fileurl: fileurl
		}
		const fileURL = fileurl;
		var a = document.createElement('a');
		a.href = fileURL;
		a.download = filename;
		a.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

		setTimeout(function () {
			// For Firefox it is necessary to delay revoking the ObjectURL
			window.URL.revokeObjectURL(fileURL);
			a.remove();
		}, 100);

	}

	getTeachers() {
		this.service.getTeachers().subscribe(
			(response: any) => {
				this.teachersArray = response.message;
				// console.log("this.categoriesArray ", this.TeachersArray)
			},
			err => {
				console.log("err", err)
			}
		);
	}

	getCategories() {
		this.service.getCategories().subscribe(
			(response: any) => {
				this.categoriesArray = response.message;
				// console.log("this.categoriesArray ", this.categoriesArray)
			},
			err => {
				console.log("err", err)
			}
		);
	}


	showmoreMusic(event: any) {
		$(".week-class.hide").each(function (n) {
			if (n + 1 <= 3) {
				$(this).show().removeClass('hide');
			}
		});
		if ($(".week-class.hide").length > 0) {
			$('.show-more').show();
		} else {
			$('.show-more').hide();

		}
	}


	viewclass(post: any) {
		const $this = this;
		this.viewclassarray = post
		if (post) {
			if (post.categories.stype == 1) {


				this.sessionDatearray = [];
				var startDate = new Date(post.start_date);
				for (var i = 0; i < (post.number_of_week + 1); i++) {
					if (i == 0) {
						var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

					} else {
						var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

					}
					let formatdate = this.formatDate(date);
					if (formatdate >= this.formatDate(new Date())) {
						this.sessionDatearray.push(formatdate);
					}
				}

			} else {
				this.getSubCategoriesbyidn(post);
			}

		}

	}



	getSubCategoriesbyidn(post: any) {
		this.sessionDatearray = [];
		if (post.subCategories_id) {
			const data = {
				id: post.subCategories_id
			}
			this.service.getSubCategoriesbyid(data).subscribe(
				(response: any) => {
					let no_of_repetitions = response.message.no_of_repetitions;
					var startDate = new Date(post.start_date);
					for (var i = 0; i < no_of_repetitions; i++) {
						if (i == 0) {
							var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

						} else {
							var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

						}
						let formatdate = this.formatDate(date);
						this.sessionDatearray.push(formatdate);
					}


				}, (err: any) => {
					console.log(err)
				}
			);


		}


	}

	formatDate(date: any) {
		var d = new Date(date),
			month = '' + (d.getUTCMonth() + 1),
			day = '' + d.getUTCDate(),
			year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [year, month, day].join('-');
	}


	subcatListing() {
		this.service.subcatListing().subscribe(
			(response: any) => {

				this.subcategorylistData = this.subArray = response.data;

			}, (err: any) => {
				console.log(err)
			}
		)
	}

	formatTime(time: any) {
		return moment(time, 'hh:mm').format('LT');
	}

	OnPageEvent(event: PageEvent) {
		const startIndex = event.pageIndex * event.pageSize;
		let endIndex = startIndex + event.pageSize;
		if (endIndex > this.totalclassbyuserArray.length) {
			endIndex = this.totalclassbyuserArray.length;
		}
		this.Index = startIndex + 1;
		this.classbyuserArray = this.totalclassbyuserArray.slice(startIndex, endIndex);
	}

	cancelSubscription(event:any,class_id: any) {
		event.currentTarget.setAttribute("disabled", "disabled");

		const data = {
			class_id: class_id
		}
		this.service.cancelSubscription(data).subscribe(
			(response: any) => {
				if (response.error == 0) {
					Swal.fire({
						title: 'Success',
						text: "Subscription cancel successfully",
						icon: 'success',
						confirmButtonColor: '#738c72',
					}).then((result) => {
						this.getClassesByuser();

					});
				}
			}, (err: any) => {
				console.log(err)
			}
		)
	}

	pauseplaySubscription(event:any,class_id: any, pause: any) {
		event.currentTarget.setAttribute("disabled", "disabled");
		if(pause == 1){
			pause = 0;
		}else{
			pause = 1;
		}
		const data = {
			class_id: class_id,
			pause: pause
		}
		this.service.pauseplaySubscription(data).subscribe(
			(response: any) => {
				if (response.error == 0) {
					if(response.pause == 1){
						var msg = "Subscription Pause successfully";
					}else{
						var msg = "Subscription Resume successfully";
					}
					Swal.fire({
						title: 'Success',
						text: msg,
						icon: 'success',
						confirmButtonColor: '#738c72',
					}).then((result) => {
						this.getClassesByuser();

					});
			

				}
			}, (err: any) => {
				console.log(err)
			}
		)
	}
}
