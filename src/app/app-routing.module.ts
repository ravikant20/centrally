import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { LoginComponent } from './auth/login/login.component';
import { TeachersComponent } from './teachers/teachers.component';
import { ClassesComponent } from './classes/classes.component';
import { BookingsComponent } from './bookings/bookings.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './auth/register/register.component';
import { MyBookingsComponent } from './my-bookings/my-bookings.component';
import { MyPaymentsComponent } from './my-payments/my-payments.component';
import { MySavecardsComponent } from './my-savecards/my-savecards.component';

import { IndexComponent } from './index/index.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ScholarshipComponent } from './scholarship/scholarship.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LoginComponent as adminLogin } from './admin/login/login.component' ;
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { AdmincategoriesComponent} from './admin/admincategories/admincategories.component';
import { AdminclassesComponent } from './admin/adminclasses/adminclasses.component';
import { MymusicComponent } from './admin/mymusic/mymusic.component';
import { ManageBookingsComponent } from './admin/manage-bookings/manage-bookings.component';
import {ManagePaymentsComponent} from './admin/manage-payments/manage-payments.component';
import { TutorsComponent } from './admin/tutors/tutors.component';

import {MindfulComponent} from './mindful/mindful.component';
import {AllCategoriesComponent} from './all-categories/all-categories.component';
import { PaymentComponent } from './payment/payment.component';

import { MindfulMusiciansComponent } from './mindful-musicians/mindful-musicians.component';
import {NeurodynamicMusicTherapyComponent } from './neurodynamic-music-therapy/neurodynamic-music-therapy.component';
import { RootedPrivateLessonsComponent } from './rooted-private-lessons/rooted-private-lessons.component';
import {AdminForgotPasswordComponent} from './admin/forgot-password/forgot-password.component';
import {UsersComponent} from './admin/users/users.component';
import {SubcategoriesComponent} from './admin/subcategories/subcategories.component';

const routes: Routes = [
  
  { path: '', component: IndexComponent},
  { path: 'login/:name', component: LoginComponent},
  { path: 'login', component: LoginComponent},
  { path: 'about-us', component: AboutUsComponent},
  { path: 'scholarship', component:ScholarshipComponent},
  { path: 'dashboard', component:DashboardComponent},
  
  { path: 'forgot-password', component:ForgotPasswordComponent},
  { path: 'profile', component:ProfileComponent},
  { path: 'categories', component: CategoriesComponent },
  { path: 'teachers', component: TeachersComponent },
  { path: 'classes', component: ClassesComponent },
  { path: 'bookings', component: BookingsComponent },
  { path: 'home', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'register/:name', component: RegisterComponent},

  { path: 'my-bookings', component: MyBookingsComponent },
  { path: 'my-payments', component: MyPaymentsComponent },
  { path: 'my-savecards', component: MySavecardsComponent },

  { path: 'change-password', component: ChangePasswordComponent },
  { path: 'reset-password', component:ResetPasswordComponent },
  { path: 'contact-us', component:ContactUsComponent },
  { path: 'mindful', component:MindfulComponent },
  { path: 'all-categories/:name',component:AllCategoriesComponent},
  
  { path: 'mindful-musicians',component:MindfulMusiciansComponent},
  { path:'neurodynamic-music-therapy', component:NeurodynamicMusicTherapyComponent},
  {path:'rooted-private-lessions',component:RootedPrivateLessonsComponent},  
  { path: 'payment',component:PaymentComponent},
  // admin
  { path: 'admin/login', component:adminLogin },
  { path: 'admin/dashboard',component:AdmindashboardComponent},
  { path: 'admin/categories',component:AdmincategoriesComponent},
  { path: 'admin/classes',component:AdminclassesComponent},
  { path: 'admin/myMusic',component:MymusicComponent},
  { path: 'admin/manageBookinngs', component:ManageBookingsComponent},
  { path: 'admin/managePayments',component:ManagePaymentsComponent},
  { path: 'admin/tutors',component:TutorsComponent},
  { path: 'admin/forgot-password',component:AdminForgotPasswordComponent},
  { path: 'admin/users',component:UsersComponent},
  { path: 'admin/subcategories',component:SubcategoriesComponent},


  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }