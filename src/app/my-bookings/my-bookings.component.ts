import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';
import { PageEvent } from '@angular/material/paginator';
import * as moment from 'moment';

@Component({
  selector: 'app-my-bookings',
  templateUrl: './my-bookings.component.html',
  styleUrls: ['./my-bookings.component.css']
})
export class MyBookingsComponent implements OnInit {

  bookingArray: any = [];
  bookingsArray: any = [];
  norecordfound: any = false;
  totalbookingArray: any = [];
  length: any = 10;
  pageSize: any = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  Index: any = 1;
  recent_bookings_data: any = [];
  sessionDatearray: any = [];
  subcategorylistData: any;
  subArray: any;

  constructor(private service: ServiceFileService, private router: Router) { }

  ngOnInit(): void {
    if (!localStorage.getItem("isAuthenticated")) {
      this.router.navigate(['/login'])

    }
    this.getMyBookngs();

    $('.full_page_loader_app').show();
    $('.my-booking-table').css('opacity', 0).hide();

  }

  getMyBookngs() {
    this.service.myBookings().subscribe(
      (response: any) => {
        //  console.log(response);
        this.totalbookingArray = response.message;
        this.bookingArray = response.message.slice(0, 10);
        if (this.bookingArray.length == 0) {
          this.norecordfound = true;
        } else {
          this.norecordfound = false;

        }
        $('.full_page_loader_app').hide();
        $('.my-booking-table').css('opacity', 1).show();

      },
      err => {
        console.log("err", err)
      }
    );
  }

  viewBookings(post: any) {
    this.bookingsArray = post;

    //console.log("View Booking details",this.bookingsArray.title);

    this.recent_bookings_data = {
      title: this.bookingsArray.classes.title,
      categories: this.bookingsArray.categoriesData,
      categories_id: this.bookingsArray.classes.categories_id,
      subCategories_id: this.bookingsArray.classes.subCategories_id,
      numberOfUsers: this.bookingsArray.number_of_users,
      start_date: this.bookingsArray.classes.start_date,
      start_time: this.bookingsArray.classes.start_time,
      end_time: this.bookingsArray.classes.end_time,
      teachers_name: this.bookingsArray.teachers_name,
      amount: this.bookingsArray.amount,
      stype: this.bookingsArray.categoriesData.stype,
      siblingsData: this.bookingsArray.siblingsData,
      issiblingsData: this.bookingsArray.issiblingsData,
      individual: this.bookingsArray.individual
    }

    if (post) {

      if (post.categoriesData.stype == 1) {


        this.sessionDatearray = [];
        var startDate = new Date(post.classes.start_date);
        for (var i = 0; i < (post.number_of_week + 1); i++) {
          if (i == 0) {
            var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

          } else {
            var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

          }
          let formatdate = this.formatDate(date);
          if(formatdate >= this.formatDate(new Date())){
            this.sessionDatearray.push(formatdate);
           }
        }
 
      } else {
        this.getSubCategoriesbyidn(post.classes);
      }



    }
    // console.log(this.recent_bookings_data);
  }


  getSubCategoriesbyidn(post: any) {
    this.sessionDatearray = [];
    if (post.subCategories_id) {
      const data = {
        id: post.subCategories_id
      }
      this.service.getSubCategoriesbyid(data).subscribe(
        (response: any) => {
          let no_of_repetitions = response.message.no_of_repetitions;

          var startDate = new Date(post.start_date);
          for (var i = 0; i < no_of_repetitions; i++) {
            if (i == 0) {
              var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

            } else {
              var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

            }
            let formatdate = this.formatDate(date);
            this.sessionDatearray.push(formatdate);
          }

        }, (err: any) => {
          console.log(err)
        }
      );


    }

  }




  formatDate(date: any) {
    var d = new Date(date),
      month = '' + (d.getUTCMonth() + 1),
      day = '' + d.getUTCDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  formatTime(time: any) {
    return moment(time, 'hh:mm').format('LT');
  }

  subcatListing() {
    this.service.subcatListing().subscribe(
      (response: any) => {

        this.subcategorylistData = this.subArray = response.data;

      }, (err: any) => {
        console.log(err)
      }
    )
  }

  OnPageEvent(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.totalbookingArray.length) {
      endIndex = this.totalbookingArray.length;
    }
    this.Index = startIndex + 1;
    this.bookingArray = this.totalbookingArray.slice(startIndex, endIndex);
  }
}
