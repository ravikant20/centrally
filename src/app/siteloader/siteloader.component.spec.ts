import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteloaderComponent } from './siteloader.component';

describe('SiteloaderComponent', () => {
  let component: SiteloaderComponent;
  let fixture: ComponentFixture<SiteloaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteloaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
