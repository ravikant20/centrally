import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { CalendarModule } from 'primeng/calendar';
@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ClassesComponent implements OnInit {
  classArray: any = [];
  categoriesArray: any = [];
  TeachersArray: any = [];
  booleanUpdate: any = false
  activeId:any = "";
  fd = new FormData();
  file:any;
  // start_date: Date = new Date();
  // end_date: Date = new Date();
  // start_time = "";
  // end_time="";
  // type="";
  // capacity:number = 0;
  // title:string = "";
  // teacher:string="";
  // categories:string="";
  data: any = [];
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    this.getClasses()
    this.getTeachers();
    this.getCategories();
  }

  getClasses() {
    this.service.getClasses().subscribe(
      (response: any) => {
        this.classArray = response.message;
        // console.log("this.categoriesArray ", this.classArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

  onFileSelected(event: any) {
    let fileList: FileList = event.target.files;
    let file: File = fileList[0];
    this.file = fileList[0];
    //this.fd.append('file', file, file.name);
    // this.selectedFile = event.target.files[0];
  }

  getTeachers() {
    this.service.getTeachers().subscribe(
      (response: any) => {
        this.TeachersArray = response.message;
        // console.log("this.categoriesArray ", this.TeachersArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

  getCategories() {
    this.service.getCategories().subscribe(
      (response: any) => {
        this.categoriesArray = response.message;
        // console.log("this.categoriesArray ", this.categoriesArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }


  changeStatus(post: any, event: any) {
    // console.log(event.srcElement.checked)
    var status = 0;
    if (event.srcElement.checked) {
      if (confirm('Are you sure you want to Active this teacher')) {
        // alert('Thing was saved to the database.');
        status = 1;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    else {
      if (confirm('Are you sure you want to inctive this teacher')) {
        status = 0;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    var daa = {
      id: post.id,
      status: status
    }

    this.service.updateTeachersStatus(daa).subscribe(
      (response: any) => {
        this.getClasses()
        alert("Status changed")
      },
      err => {
        console.log("err", err)
      }
    );
  }

  SaveClass() {
    const formdate = new FormData();
    if (this.data.type == '1') {
      this.data.capacity = 1;
    }

    if (!this.booleanUpdate) {
      this.data.start_date = new Date(this.data.start_date);
      this.data.start_date = this.data.start_date.getFullYear().toString().padStart(2, "0") + "-" + this.data.start_date.getUTCMonth().toString().padStart(2, "0") + "-" + this.data.start_date.getDate().toString().padStart(2, "0")

      this.data.end_date = new Date(this.data.end_date);
      this.data.end_date = this.data.end_date.getFullYear().toString().padStart(2, "0") + "-" + this.data.end_date.getUTCMonth().toString().padStart(2, "0") + "-" + this.data.end_date.getDate().toString().padStart(2, "0")


      this.data.start_time = new Date(this.data.start_time);
      this.data.start_time = this.data.start_time.getUTCHours().toString().padStart(2, "0") + ":" + this.data.start_time.getUTCMinutes().toString().padStart(2, "0") + ":" + this.data.start_time.getUTCSeconds().toString().padStart(2, "0")

      this.data.end_time = new Date(this.data.end_time);
      this.data.end_time = this.data.end_time.getUTCHours().toString().padStart(2, "0") + ":" + this.data.end_time.getUTCMinutes().toString().padStart(2, "0") + ":" + this.data.end_time.getUTCSeconds().toString().padStart(2, "0")

      var post = {
        category: this.data.categories,
        class_type: this.data.type,
        teacher: this.data.teacher,
        capacity: this.data.capacity,
        start_date: this.data.start_date,
        end_date: this.data.end_date,
        end_time: this.data.end_time,
        start_time: this.data.start_time,
        title: this.data.title,
      }
      if(this.file){
        formdate.append('file', this.file, this.file.name);
      }
      
      formdate.append('category', this.data.categories)
      formdate.append('class_type', this.data.type)
      formdate.append('teacher', this.data.teacher)
      formdate.append('capacity', this.data.capacity)
      formdate.append('start_date', this.data.start_date)
      formdate.append('end_date', this.data.end_date)
      formdate.append('end_time', this.data.end_time)
      formdate.append('start_time', this.data.start_time)
      formdate.append('title', this.data.title)
      formdate.append('payment_one_student', this.data.payment_one_student)
      formdate.append('payment_one_plus_student', this.data.payment_one_plus_student)
      formdate.append('payment_two_plus_student', this.data.payment_two_plus_student)
      this.service.addClass(formdate).subscribe(
        (response: any) => {
          this.data = [];
          this.getClasses();
          this.booleanUpdate = false;
        },
        err => {
          console.log("err", err)
        }
      );
    } else {
      if (this.data.type == '1') {
        this.data.capacity = 1;
      }

      this.data.start_date = new Date(this.data.start_date);
      this.data.start_date = this.data.start_date.getFullYear().toString().padStart(2, "0") + "-" + this.data.start_date.getUTCMonth().toString().padStart(2, "0") + "-" + this.data.start_date.getDate().toString().padStart(2, "0")

      this.data.end_date = new Date(this.data.end_date);
      this.data.end_date = this.data.end_date.getFullYear().toString().padStart(2, "0") + "-" + this.data.end_date.getUTCMonth().toString().padStart(2, "0") + "-" + this.data.end_date.getDate().toString().padStart(2, "0")


      this.data.start_time = new Date(this.data.start_time);
      this.data.start_time = this.data.start_time.getUTCHours().toString().padStart(2, "0") + ":" + this.data.start_time.getUTCMinutes().toString().padStart(2, "0") + ":" + this.data.start_time.getUTCSeconds().toString().padStart(2, "0")

      this.data.end_time = new Date(this.data.end_time);
      this.data.end_time = this.data.end_time.getUTCHours().toString().padStart(2, "0") + ":" + this.data.end_time.getUTCMinutes().toString().padStart(2, "0") + ":" + this.data.end_time.getUTCSeconds().toString().padStart(2, "0")


      var po = {
        category: this.data.categories,
        class_type: this.data.type,
        teacher: this.data.teacher,
        capacity: this.data.capacity,
        start_date: this.data.start_date,
        end_date: this.data.end_date,
        end_time: this.data.end_time,
        start_time: this.data.start_time,
        title: this.data.title,
        id: this.activeId,
      }
      formdate.append('category', this.data.categories)
      formdate.append('class_type', this.data.type)
      formdate.append('teacher', this.data.teacher)
      formdate.append('capacity', this.data.capacity)
      formdate.append('start_date', this.data.start_date)
      formdate.append('end_date', this.data.end_date)
      formdate.append('end_time', this.data.end_time)
      formdate.append('start_time', this.data.start_time)
      formdate.append('title', this.data.title)
      formdate.append('id', this.activeId)
      formdate.append('payment_one_student', this.data.payment_one_student)
      formdate.append('payment_one_plus_student', this.data.payment_one_plus_student)
      formdate.append('payment_two_plus_student', this.data.payment_two_plus_student)
      if(this.file){
        formdate.append('file', this.file, this.file.name);
      }
      this.service.updateClass(formdate).subscribe(
        (response: any) => {
          this.data = [];
          this.getClasses();
        },
        err => {
          console.log("err", err)
        }
      );
    }

  }

  updateData(post: any, id: any) {
    this.booleanUpdate = true;
    var todayDate = new Date().toISOString().slice(0, 10);
    this.data.categories = post.categories_id;
    this.data.teacher = post.teachers_id;
    this.data.class_type = post.class_type;
    this.data.start_date = new Date(post.start_date);
    this.data.end_date = new Date(post.end_date);
    this.data.start_time = new Date(todayDate + " " + post.start_time);
    this.data.end_time = new Date(todayDate + " " + post.end_time);
    this.data.title = post.title;
    this.data.capacity = post.capacity;
    this.data.type = post.class_type;
    this.data.payment_one_student = post.payment_one_student;
    this.data.payment_one_plus_student = post.payment_one_plus_student;
    this.data.payment_two_plus_student = post.payment_two_plus_student;
    this.data.end_date = new Date(post.end_date);
    this.activeId = id;
  }

  deleteClasses(id: any) {

  }
}
