import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';
import * as moment from 'moment';
import * as $ from 'jquery';
import Swal from 'sweetalert2';


@Component({
	selector: 'app-all-categories',
	templateUrl: './all-categories.component.html',
	styleUrls: ['./all-categories.component.css'],
	encapsulation: ViewEncapsulation.None,
})
export class AllCategoriesComponent implements OnInit {
	value: Date = new Date();
	paramId: any = "";
	subCategoriesArray: any = [];
	CategoriesArray: any = [];
	arraySiblings: any = [];
	siblingsName: any = ""
	showHidesiblingfield: boolean = false
	subCategoriesid: any = "";
	showAddSiblingfield: boolean = true;
	$dds: any = false
	formatedTimeSlot1: any = [];
	formatedTimeSlot2: any = [];
	className: any = [];
	classNameArray: any = [];
	tutorName: any = [];
	tutorNameArray: any = [];
	classname: any = [];
	classidArray: any = [];
	time1: any = [];
	time2: any = [];
	todayDay: any = ""
	date1: any = "";
	categoriEsteacherWise: any = [];
	allteacher: any = [];
	activeTeacher: any = "";
	activeTeacher2: any = ""
	ageGroup: string = " Years";
	searchText: string = "";
	error_mindful: string = "";
	showtotaldefaultslot: any = 12;
	category_id: any = "";
	urlParameter: any = "";
	classidval: any = "";
	slotval: any = "";
	arraySiblingsNeurodynamic: any = [];
	showAddSiblingfieldNeurodynamic: boolean = true;
	showHidesiblingfieldNeurodynamic: boolean = false;
	siblingsNameNeurodynamic: any = "";
	error_neuro: string = "";
	appendafterRemovesibling: boolean = false;
	arraySiblingsRooted: any = [];
	showAddSiblingfieldRoted: boolean = true;
	showHidesiblingfieldRooted: boolean = false;
	siblingsNameRooted: string = "";
	error_rooted: string = "";
	dates: Date[] = [new Date()];
	classesDatesArray: any = [];
	validDate: Date[] = [new Date()];
	classDate: any = [];
	cdate = new Date();
	newarrayt: any = [];
	interval: any = "";
	number_of_users: any = "";
	cd: any = this.cdate.getUTCDate();
	cm: any = this.cdate.getUTCMonth();
	cy: any = this.cdate.getUTCFullYear();
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];

	dayNames: any = [
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		"Sunday"
	];

	constructor(private route: ActivatedRoute, private router: Router, private service: ServiceFileService) {



		this.route.queryParams.subscribe(params => {
			this.paramId = params['id'];
		});

		this.urlParameter = this.route.snapshot.paramMap.get('name');

		this.siblingsName = localStorage.getItem("name");
		this.siblingsNameNeurodynamic = localStorage.getItem("name");
	}

	ngOnInit(): void {
		if (!localStorage.getItem("isAuthenticated")) {
			this.router.navigate(['/login'])

		}
		window.scrollTo(0, 0);
		var today = new Date();
		var day = String(today.getMonth() + 1)
		if (day.toString().length == 1) {
			day = "0" + day;
		}

		this.todayDay = today.getFullYear() + '-' + day + '-' + today.getDate();
		$('.full_page_loader_app').show();
		$('.booking-inn').css('opacity', 0).hide();

		this.getCategories();
		this.getSubCategories();

		switch (this.urlParameter) {
			case "mindful":
				var catid = 1;
				break;
			case "neurodynamic":
				var catid = 2;
				break;
			case "rooted":
				var catid = 3;
				break;
			default:
				var catid = 1;
		}

		this.category_id = catid;

		// this.getSlotsandTeacher();
		this.scriptforall();
		// this.getSlotBycategory();
	}

	openClanedar() {
		var click = document.getElementById('openCalen')
		if (click) {
			click.click()
		}
	}

	getCategories() {
		this.service.getCategories().subscribe(
			(response: any) => {
				this.CategoriesArray = response.message;
			},
			err => {
				console.log("err", err)
			}
		);
	}

	setCategoryId(id: any) {
		if (id == "") {
			this.category_id = 1;
		} else if (id == 3) {
			this.category_id = id;
			let cloneactive = document.querySelectorAll('.clone.active');
			for (let i = 0; i < cloneactive.length; i++) {
				cloneactive[i].remove();
			}
		} else {
			this.category_id = id;
		}

	}

	getSubCategories() {
		let data = {
			searchValue: this.searchText
		}
		this.service.getSubCategories(data).subscribe(
			(response: any) => {
				this.subCategoriesArray = response.message;

				$('.full_page_loader_app').hide();
				$('.booking-inn').css('opacity', 1).show();


			},
			err => {
				console.log("err", err)
			}
		);
	}


	addSiblings(event: any) {
		this.addSiblingsInputnew(event)
	}

	addSiblingsInputnew(event: any) {
		const $this = this;
		$('ul li.clone').find('span.text-danger').text("");
		var classrequired = "";
		$('ul li.clone').each(function () {
			if ($(this).find('input.sibling_name').val() == "") {
				classrequired += true;
			}
		});


		if (!classrequired) {
			var newarray: any = [];
			$('.sibling_name').each(function () {
				if ($(this).val() != "") {
					newarray.push($(this).val());
				}
			});


			$('<li class="clone"><input type="text" class="form-control sibling_name" value="" name="siblingsName[]" required><span class="close-sb"  style="cursor: pointer;"><i class="fa fa-minus-circle" style="color:#738c72"></i></span><span  class="text-danger"></span></li>').insertBefore($(event.target).closest('.add-sibling'));
			$(event.target).closest('.add-sibling').prev('li.clone').addClass('active');
		} else {
			$('ul li.clone').each(function () {
				if ($(this).find('input.sibling_name').val() == "") {
					$(this).find('span.text-danger').text("Please enter sibling name");
				}
			});

		}
		this.remove_sibling();

	}




	selectDate(event: any) {


		var today = new Date(event);
		var daysss = String(today.getMonth() + 1)
		if (daysss.toString().length == 1) {
			daysss = "0" + daysss;
		}
		this.todayDay = today.getFullYear() + '-' + daysss + '-' + today.getDate();
		this.getSlotsandTeacher();

		this.validDate = [today];
		$('.error-msg').removeAttr('id').html('');



	}



	getSlotsandTeacher() {
		const $this = this;
		if ($('ul.nav .nav-item .nav-link.active').data('value')) {
			this.category_id = $('ul.nav .nav-item .nav-link.active').data('value');

		} else {
			this.category_id = 1;
		}
		if ($('.radio_subcat:checked').val()) {
			this.subCategoriesid = $('.radio_subcat:checked').val();
		} else {
			this.subCategoriesid = 1
		}
		var data = {
			date: this.todayDay,
			categories_id: this.category_id,
			subCategories_id: this.subCategoriesid

		}


		this.service.getRecordsWithCategories(data).subscribe(
			(response: any) => {

				if (response.status == 1) {
					if (response.message != "") {
						this.categoriEsteacherWise = response.message
						this.formatedTimeSlot1 = [];
						this.classNameArray = [];
						this.tutorNameArray = [];
						this.classidArray = [];

						for (var i = 0; i < this.categoriEsteacherWise.length; i++) {

							this.time1 = this.categoriEsteacherWise[i].start_time;
							this.time2 = this.categoriEsteacherWise[i].end_time;
							this.className = this.categoriEsteacherWise[i].title;
							this.tutorName = this.categoriEsteacherWise[i].teachers_name;
							this.classNameArray.push(this.className);
							this.tutorNameArray.push(this.tutorName);
							this.formatedTimeSlot1.push(moment(this.time1, 'hh:mm').format('LT') + " to " + moment(this.time2, 'hh:mm').format('LT'));
							this.classidArray.push(this.categoriEsteacherWise[i].id);

						}

						this.classidval = this.classidArray[0];

						setInterval(function () {
							let fulldate = new Date($this.todayDay);
							let y: any = fulldate.getUTCFullYear();
							let d: any = fulldate.getUTCDate();
							let m: any = $this.monthNames[fulldate.getUTCMonth()];

							$('#data-found' + $this.category_id + $this.subCategoriesid + ' .p-datepicker-calendar tbody tr td').each(function () {
								if (!$(this).hasClass('p-datepicker-other-month')) {
									let monthhtml: any = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-month').text();
									let yearhtml: any = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-year').text();

									if (parseInt($(this).find('span').text()) == parseInt(d) && monthhtml.toLowerCase() == m.toLowerCase() && yearhtml == y) {
										if (!$(this).find('span').hasClass('p-highlight')) {
											$(this).find('span').addClass('p-highlight');


										}
									}
								}
							});

						});


					} else {
						this.categoriEsteacherWise = "";
						this.classNameArray = [];
						this.tutorNameArray = [];
						this.formatedTimeSlot1 = [];
					}


				} else {
					console.log('error');
				}



			},
			err => {
				console.log("err", err)
			}
		);
	}


	// changeCategory(id: any) {
	//   this.paramId = id


	//   var element1 = document.getElementById('mindful1');
	//   if (element1) {
	//     element1.style.display = "none"
	//   }

	//   var element2 = document.getElementById('mindful3');
	//   if (element2) {
	//     element2.style.display = "none"
	//   }
	//   var element3 = document.getElementById('mindful7');
	//   if (element3) {
	//     element3.style.display = "none"
	//   }

	//   var main = document.getElementById('mindful' + id)
	//   if (main) {
	//     main.style.display = "block"
	//   }
	//   this.arraySiblings = [];
	// }



	getSlotBycategory($pid: any, $sid: any) {

		const $this = this;
		$('.singledate').removeAttr('id');
		// this.date1 = new Date();
		this.categoriEsteacherWise = []
		this.formatedTimeSlot1 = []

		var data = {
			date: this.todayDay,
			categories_id: $pid,
			subCategories_id: $sid
		}


		this.service.getSlotBycategory(data).subscribe(
			(response: any) => {

				this.classesDatesArray = response.message;
				this.newarrayt = [];
				var fulldatenewarray: any = [];

				if (response.message) {
					if ($pid == 1) {
						this.formatedTimeSlot1 = [];
						this.classNameArray = [];
						this.tutorNameArray = [];
						this.classidArray = [];
						this.classDate = [];
					}
					for (var i = 0; i < this.classesDatesArray.length; i++) {
						if ($pid == 1) {
							this.time1 = this.classesDatesArray[i].start_time;
							this.time2 = this.classesDatesArray[i].end_time;
							this.className = this.classesDatesArray[i].title;
							this.tutorName = this.classesDatesArray[i].teachers_name;
							this.classNameArray.push(this.className);
							let fulldate = new Date(this.classesDatesArray[i].start_date);
							this.classDate.push({ date: this.classesDatesArray[i].start_date, day: $this.dayNames[fulldate.getUTCDay()] });
							this.tutorNameArray.push(this.tutorName);
							this.formatedTimeSlot1.push(moment(this.time1, 'hh:mm').format('LT') + " to " + moment(this.time2, 'hh:mm').format('LT'));
							this.classidArray.push(this.classesDatesArray[i].id);
						}

						var startdate = this.classesDatesArray[i].start_date;
						let fulldate = new Date(startdate);
						fulldatenewarray.push(fulldate);
						let y = fulldate.getUTCFullYear();
						let d = fulldate.getUTCDate();
						let m = $this.monthNames[fulldate.getUTCMonth()];
						let cnewarray = { 'y': y, 'm': m, 'd': d };
						this.newarrayt.push(cnewarray);

						this.validDate.push(fulldate);

					}
					this.classidval = this.classidArray[0];


				}
				$('.singledate').attr('id', '');

				if (this.newarrayt.length > 0) {
					$('.singledate').attr('id', 'data-found' + $pid + $sid);
				}




				// this.invalidDates = fulldatenewarray;


				this.interval = setInterval(function () {

					if ($this.newarrayt.length > 0) {

						for (var $i = 0; $i < $this.newarrayt.length; $i++) {

							var d = ($this.newarrayt[$i].d ? $this.newarrayt[$i].d : '');

							var y = ($this.newarrayt[$i].y ? $this.newarrayt[$i].y : '');
							var m = ($this.newarrayt[$i].m ? $this.newarrayt[$i].m : '');


							$('#data-found' + $pid + $sid + ' .p-datepicker-calendar tbody tr td').each(function () {
								if (!$(this).hasClass('p-datepicker-other-month')) {
									let monthhtml = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-month').text();
									let yearhtml = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-year').text();

									if (parseInt($(this).find('span').text()) == parseInt(d) && monthhtml.toLowerCase() == m.toLowerCase() && yearhtml == y) {
										if (!$(this).hasClass('cactive')) {
											$(this).addClass('cactive');


										}
									}

								}
							});


						}
					}
				});



			},
			err => {
				console.log("err", err)
			}
		)

	}

	scriptforall() {

		var $this = this;

		$(document).on('click', '.nav-tabs .nav-link', function () {
			$('.singledate').removeAttr('id');
			$('.radio_subcat').prop('checked', false);;
			// $this.date1 = new Date();
			$this.categoriEsteacherWise = ''
			$this.formatedTimeSlot1 = ''
			$('.singledate').removeClass('subcat-selected');
			$('.error-msg').removeAttr('id').html('');


		});

		$(document).on('click', '.radio_subcat', function () {
			let sid = $(this).val();
			let pid = $('ul.nav .nav-item .nav-link.active').data('value');
			$('.singledate').addClass('subcat-selected');
			clearInterval($this.interval);
			$this.date1 = "";
			$this.getSlotBycategory(pid, sid);
			$('.error-msg').removeAttr('id').html('');



		});

		$(document).on('click', '.time-slot', function () {
			$('.time-slot').find('.radio_slot').prop('checked', false);
			$(this).find('.radio_slot').prop('checked', true);
			$this.classidval = $(this).find('.class_id_slot').val();
			$this.slotval = $(this).find('.radio_slot').val();



		});



		// allow only alphabatics and white space.
		$(document).on('keypress', '.sibling_name', function (e: any) {
			var regex = new RegExp(/^[a-zA-Z\s]+$/);
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else {
				e.preventDefault();
				return false;
			}

		});

		// removeclass from today date in p-calander.
		setInterval(function () {
			$('.p-datepicker-calendar tbody tr td.p-datepicker-today').each(function () {
				$(this).find('span').removeClass('p-highlight');
				$(this).removeClass('p-datepicker-today');

			});

			$('.p-datepicker-calendar tbody tr td').each(function () {
				if (!$(this).hasClass('cactive')) {
					$(this).find('span').removeClass('p-highlight');

				}

			});

		});

	}

	book_now() {
		$('.error-msg').removeAttr('id').html('');
		let sid = $('.radio_subcat:checked').val();

		let pid = $('ul.nav .nav-item .nav-link.active').data('value');

		let book_slot_form = $('.book_slot_form').serialize();

		let cuserid = localStorage.getItem("ID");
		const cloneinputsiblingarray: any = [];
		$('.clone').each(function () {
			if (!$(this).find('input.sibling_name').val()) {
				$(this).find('span.text-danger').text("Please enter sibling name");

			} else {
				cloneinputsiblingarray.push($(this).find('input.sibling_name').val());
			}
		});

		if (!$('.radio_subcat:checked').val()) {
			let text = $('.tab-pane.active').find('div.select-class h3').text();

			$('.error-msg').attr('id', 'show').html('Please ' + text);

		}
		else if (!$('.radio_slot').val()) {

			$('.error-msg').attr('id', 'show').html('Please Select a Class');

		}
		if ($('.radio_slot').val() && cloneinputsiblingarray.length > 0) {

			this.number_of_users = cloneinputsiblingarray.length;
			let formdata = book_slot_form + '&pid=' + pid + '&sid=' + sid + '&user_id=' + cuserid;

			this.checkuserlimitforclass(this.classidval, this.number_of_users, sid, formdata);

		}

	}

	checkuserlimitforclass(class_id: any, number_of_users: any, sid: any, formdata: any) {
		const data = {
			'class_id': class_id,
			'number_of_users': number_of_users
		}

		this.service.checkuserlimitforclass(data).subscribe(
			(response: any) => {
				if (response.error == 0 && response.message == true && response.aviable == 0) {

					$('.error-msg').attr('id', 'show').html("This class has reached maximum users limit. Please select another class");

				} else if (response.error == 0 && response.message == true && response.aviable > 0) {
					$('.error-msg').attr('id', 'show').html("There are only " + response.aviable + " slots left. Please  update the users added.");
				}
				else {


					this.getSubCategoriesbyidn(sid, formdata);
				}
			});

	}

	remove_sibling() {
		$(document).on('click', ".close-sb", function () {
			$(this).closest('li').remove();
		})
	}


	showmoreSlot(event: any) {
		const $this = this;
		$(".time-slot.hide").each(function (n: any) {
			if (parseInt(n + 1) <= $this.showtotaldefaultslot) {
				$(this).removeClass('hide');
			}
		});
		if ($(".time-slot.hide").length > 0) {
			$('.show-more').show();
		} else {
			$('.show-more').hide();

		}
	}

	getBookingUserSubscription(class_id: any, formdata: any) {
		const data = {
			'class_id': class_id
		}
		this.service.getBookingUserSubscription(data).subscribe(
			(response: any) => {
				if (response.error == 0) {

					if (response.message && response.message.class_id) {
						Swal.fire({
							title: 'Error',
							text: "You already have a subscription for this class",
							icon: 'warning',
							confirmButtonColor: '#738c72',
						}).then((result) => {

						});
					} else {

						this.saveBookingCart(formdata);

					}
				}
			},
			err => {
				console.log("err", err)
			})
	}


	getSubCategoriesbyidn(id: any, formdata: any) {
		const data = {
			id: id
		}
		this.service.getSubCategoriesbyidn(data).subscribe(
			(response: any) => {
				if (response.error == 0) {
					const stype = response.message.stype;
					if (stype == 1) {
						this.getBookingUserSubscription(this.classidval ? this.classidval : this.classidArray ? this.classidArray[0] : '', formdata);
					} else {
						this.saveBookingCart(formdata);
					}
				}

			}, (err: any) => {
				console.log(err)
			}
		)
	}

	saveBookingCart(formdata: any) {
		this.service.saveBookingcart(formdata).subscribe(
			(response: any) => {
				var $this = this;


				$this.router.navigate(['/payment'])



			},
			err => {
				console.log("err", err)
			}

		)
	}


}
