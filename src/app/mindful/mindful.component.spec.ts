import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MindfulComponent } from './mindful.component';

describe('MindfulComponent', () => {
  let component: MindfulComponent;
  let fixture: ComponentFixture<MindfulComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MindfulComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MindfulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
