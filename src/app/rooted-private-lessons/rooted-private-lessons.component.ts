import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rooted-private-lessons',
  templateUrl: './rooted-private-lessons.component.html',
  styleUrls: ['./rooted-private-lessons.component.css']
})
export class RootedPrivateLessonsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll(0,0);
  }

}
