import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RootedPrivateLessonsComponent } from './rooted-private-lessons.component';

describe('RootedPrivateLessonsComponent', () => {
  let component: RootedPrivateLessonsComponent;
  let fixture: ComponentFixture<RootedPrivateLessonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RootedPrivateLessonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RootedPrivateLessonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
