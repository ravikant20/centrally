import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  date:any = new Date();
  cyear:any = this.date.getUTCFullYear();
  constructor() { }

  ngOnInit(): void {
  }


  scrolltotop(){
    window.scrollTo(0, 0); 
  }
}
