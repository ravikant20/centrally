import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeurodynamicMusicTherapyComponent } from './neurodynamic-music-therapy.component';

describe('NeurodynamicMusicTherapyComponent', () => {
  let component: NeurodynamicMusicTherapyComponent;
  let fixture: ComponentFixture<NeurodynamicMusicTherapyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NeurodynamicMusicTherapyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NeurodynamicMusicTherapyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
