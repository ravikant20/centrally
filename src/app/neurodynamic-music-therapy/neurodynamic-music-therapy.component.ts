import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-neurodynamic-music-therapy',
  templateUrl: './neurodynamic-music-therapy.component.html',
  styleUrls: ['./neurodynamic-music-therapy.component.css']
})
export class NeurodynamicMusicTherapyComponent implements OnInit {
loginUser:any = false;
  constructor(private router:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("isAuthenticated") && localStorage.getItem("isAuthenticated")== "Yes"){
      this.loginUser = true;

    }
    window.scroll(0,0); 
  }


  checkForRegisterNeurodynamic(value:any){
    var isAuthenticated = localStorage.getItem('isAuthenticated');
    if(isAuthenticated){
      this.router.navigate(['/all-categories',value])
    }else{
      this.router.navigate(['/register',value])
    }

  }


}
