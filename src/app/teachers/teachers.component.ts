import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {
  TeachersArray:any = [];
  teacherName = "";
  activeId = "";
  updateOrInsert: boolean = false;
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    this.getTeachers()
  }

  getTeachers(){
    this.service.getTeachers().subscribe(
      (response: any) => {
        this.TeachersArray = response.message;
        console.log("this.categoriesArray ", this.TeachersArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }


  SaveTeacher() {
    var data = {
      name: this.teacherName
    }
    if (!this.updateOrInsert) {
      this.service.addTeachers(data).subscribe(
        (response: any) => {
          this.teacherName = "";
          this.getTeachers()
          alert("Categories Inserted")
        },
        err => {
          console.log("err", err)
        }
      );
    } else {
      var daa = {
        name: this.teacherName,
        id: this.activeId
      }
      this.service.updateTeachers(daa).subscribe(
        (response: any) => {
          this.teacherName = "";
          this.updateOrInsert = false;
          this.getTeachers()
          alert("teacher Updated")
        },
        err => {
          console.log("err", err)
        }
      );
    }

  }

  updateData(name: any, id: any) {
    this.teacherName = name;
    this.updateOrInsert = true;
    this.activeId = id;
  }


  changeStatus(post: any, event: any) {
    console.log(event.srcElement.checked)
    var status = 0;
    if (event.srcElement.checked) {
      if (confirm('Are you sure you want to Active this teacher')) {
        // alert('Thing was saved to the database.');
        status = 1;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    else{
      if (confirm('Are you sure you want to inctive this teacher')) {
        status = 0;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    var daa = {
      id:post.id,
      status:status
    }

    this.service.updateTeachersStatus(daa).subscribe(
      (response: any) => {
        this.getTeachers()
        alert("Status changed")
      },
      err => {
        console.log("err", err)
      }
    );
  }


  deleteTeacher(id:any){
    if (confirm('Are you sure you want to delete this teacher')) {
      this.service.deleteTeachers({id:id}).subscribe(
        (response: any) => {
          this.getTeachers()
          alert("Teacher deleted")
        },
        err => {
          console.log("err", err)
        }
      );
    } 
  }

}
