import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  name = "";
  phone = "";
  email = "";
  subject = "";
  message = ""
  error: any = [];
  emailValid: boolean = false;
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

  contactsubmit() {
    this.error = [];
    if (this.name == "") {
      this.error.name = true;
      return
    } else {
      this.error.name = false;
    }
    if (this.email == "") {
      this.error.email = true;
      return
    } else {
      this.error.email = false;
      var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!filter.test(this.email)) {
        this.emailValid = true
        return
      }
      else {
        this.emailValid = false
      }
    }
    if (this.phone == "") {
      this.error.phone = true;
      return
    } else {
      this.error.phone = false;
    }

    if (this.subject == "") {
      this.error.subject = true;
      return
    } else {
      this.error.subject = false;
    }

    if (this.message == "") {
      this.error.message = true;
      return
    } else {
      this.error.message = false;
    }
    var data = {
      name: this.name,
      email: this.email,
      phone: this.phone,
      subject: this.subject,
      message: this.message,
    }
    this.service.contactUs(data).subscribe(
      (response: any) => {
        if(response.error == 0){
        Swal.fire({
          title: 'Success',
          text: 'We will get back to you soon',
          icon: 'success',
        })
        this.name = "";
        this.email = "";
        this.phone = "";
        this.subject = "";
        this.message = "";
      }else{
        Swal.fire({
          title: 'Error',
          text: 'Oops something went wrong!',
          icon: 'warning',
          confirmButtonText: 'OK'
        })
      }
      },
      err => {
        console.log(err)
      }
    );

  }

}
