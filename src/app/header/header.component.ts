import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loggedInUser: any = ""
  image:any = [];
  name:any = [];
	urlParameter: any = "";
  mainurl:any = environment.PROJECT_URL;


  constructor(private route: ActivatedRoute,private router: Router) { 
    this.urlParameter = this.router.url;

  }

  ngOnInit(): void {
    if (localStorage.getItem('role') == '1') {
      this.loggedInUser = "admin";
   
      this.router.navigate(['/admin/dashboard']);
    } else if (localStorage.getItem('role') == '2') {
      this.loggedInUser = "user"
    }
    else {
      this.loggedInUser = "guest"
     
    }
    if(localStorage.getItem('image')){
      this.image = localStorage.getItem('image');
    }else{
      this.image = this.mainurl+"/centrallyRooted/public/uploads/1625737388.png";
    }
    if(localStorage.getItem('name')){
      this.name = localStorage.getItem('name');
    }
    $('body').removeClass('mat-typography');
    
  }

  logout() {
    localStorage.removeItem("role")
    localStorage.removeItem("token")
    localStorage.removeItem("isAuthenticated")
    localStorage.removeItem("image")
    localStorage.removeItem("name")
    localStorage.removeItem("ID")
    this.router.navigate(['/login'])
    // if (this.router && this.router.url === '/') {
    //   window.location.reload();
    // } else {
    //   this.router.navigate(['/']);
    // }    
  }

}
