import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  categoriesArray:any = [];
  constructor(private router:Router,private service: ServiceFileService) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.getCategories()
  }

  checkForLogin(value:any){
    var isAuthenticated = localStorage.getItem('isAuthenticated');

    if(isAuthenticated){
      this.router.navigate(['/all-categories',value])
    }else{
      this.router.navigate(['/login',value])
    }
  }

  checkForLoginMindful(value:any){
    var isAuthenticated = localStorage.getItem('isAuthenticated');

    if(isAuthenticated){
      this.router.navigate(['/all-categories',value])
    }else{
      this.router.navigate(['/login',value])
    }
  }


  checkForLoginNeurodynamic(value:any){
    var isAuthenticated = localStorage.getItem('isAuthenticated');
    if(isAuthenticated){
      this.router.navigate(['/all-categories',value])
    }else{
      this.router.navigate(['/login',value])
    }

  }

  checkForLoginRooted(value:any){
    var isAuthenticated = localStorage.getItem('isAuthenticated');
    if(isAuthenticated){
      this.router.navigate(['/all-categories',value])
    }else{
      this.router.navigate(['/login',value])
    }

  }

  getCategories() {
    this.service.getCategories().subscribe(
      (response: any) => {
        this.categoriesArray = response.message;
        // console.log("this.categoriesArray ", this.categoriesArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

}
