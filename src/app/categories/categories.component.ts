import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categoriesArray: any = [];
  categoryName = "";
  activeId = "";
  updateOrInsert: boolean = false;
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    this.getCategories()
  }


  getCategories() {
    this.service.getCategories().subscribe(
      (response: any) => {
        this.categoriesArray = response.message;
        console.log("this.categoriesArray ", this.categoriesArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

  SaveCategories() {
    var data = {
      name: this.categoryName
    }
    if (!this.updateOrInsert) {
      this.service.addCategories(data).subscribe(
        (response: any) => {
          this.categoryName = "";
          this.getCategories()
          alert("Categories Inserted")
        },
        err => {
          console.log("err", err)
        }
      );
    } else {
      var daa = {
        name: this.categoryName,
        id: this.activeId
      }
      this.service.updateCategories(daa).subscribe(
        (response: any) => {
          this.categoryName = "";
          this.updateOrInsert = false;
          this.getCategories()
          alert("Categories Updated")
        },
        err => {
          console.log("err", err)
        }
      );
    }

  }

  updateData(name: any, id: any) {
    this.categoryName = name;
    this.updateOrInsert = true;
    this.activeId = id;
  }


  changeStatus(post: any, event: any) {
    console.log(event.srcElement.checked)
    var status = 0;
    if (event.srcElement.checked) {
      if (confirm('Are you sure you want to Active this category')) {
        // alert('Thing was saved to the database.');
        status = 1;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    else{
      if (confirm('Are you sure you want to inctive this category')) {
        status = 0;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    var daa = {
      id:post.id,
      status:status
    }

    this.service.updateCategoriesStatus(daa).subscribe(
      (response: any) => {
        this.getCategories()
        alert("Status changed")
      },
      err => {
        console.log("err", err)
      }
    );
  }


  deleteCategories(id:any){
    if (confirm('Are you sure you want to delete this category')) {
      this.service.deleteCategories({id:id}).subscribe(
        (response: any) => {
          this.getCategories()
          alert("Category deleted")
        },
        err => {
          console.log("err", err)
        }
      );
    } 
  }

}
