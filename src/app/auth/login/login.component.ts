import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: any = "";
  password: any = "";
  remember_me:any = ""; 
  messageError: any;
  urlParameter:any="";
  loading: boolean = false; 

  constructor(private service: ServiceFileService, private router: Router,private route:ActivatedRoute) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.urlParameter = this.route.snapshot.paramMap.get('name');
    if(localStorage.getItem("isAuthenticated") && localStorage.getItem("isAuthenticated")== "Yes"){
      this.router.navigate(['/'])

    }

    this.remember_me = localStorage.getItem("remember_me");
   const ecr =  localStorage.getItem("ecr");
   const pcr = localStorage.getItem("pcr");
    localStorage.getItem("pcr");
    this.email = (ecr?window.atob(ecr):'');
     this.password = (pcr?window.atob(pcr):'');
   if(this.remember_me == "true"){
     this.remember_me = true;
     
   }else{
    this.remember_me = false;


   }

  }

  submitLoginForm() {
    this.loading = true;
    let data = {                        //let temporary variable  //data object
      email: this.email,
      password: this.password,
    }

    this.service.login(data).subscribe(
      (response: any) => {
      
        var res = response;
        if (res.error == 1) {
          this.messageError = res.message;
        }else {
          this.messageError = "";
          localStorage.setItem("ID",res.data['id'])
          localStorage.setItem("role", res.data['role'])
          localStorage.setItem("token", res.data['token'])
          localStorage.setItem("image", res.data['image'])
          localStorage.setItem("isAuthenticated","Yes")
          localStorage.setItem("name", res.data['name'])
          localStorage.setItem("remember_me",this.remember_me);

          if(this.remember_me){
            localStorage.setItem("uiD",window.btoa(res.data['id']));
            localStorage.setItem("ecr",window.btoa(res.data['email']));
            localStorage.setItem("pcr",window.btoa(this.password));

          }

          if(this.urlParameter=="mindful"){
            this.router.navigate(['/all-categories','mindful'])
          }else if(this.urlParameter=="neurodynamic"){
            this.router.navigate(['/all-categories','neurodynamic'])
          }else if(this.urlParameter=="rooted"){
            this.router.navigate(['/all-categories','rooted'])
          }else if(this.urlParameter == 'all-categories'){
            this.router.navigate(['/all-categories','mindful'])
          }else{
            this.router.navigate(['/'])
          }
          
        }
    this.loading = false;

      }, err => {
        console.log(err)
      }
    )
  }

  remembercheckCheckBoxvalue(event:any){
    this.remember_me = event.target.checked;
  }

}
