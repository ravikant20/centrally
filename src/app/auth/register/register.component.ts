import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ServiceFileService } from 'src/service-file.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: any;
  email: any;
  last_name: any;
  phoneNumber: any;
  // type: any;
  password: any;
  confirm_password: any;
  usersArray: any;
  messageError: any;
  errorthrown: any;
  formGroup = new FormGroup({});
  error: boolean = false;
  urlParameter: any = "";
  remember_me: any = "";
  formvalid: any = false;
  disablebtn: any = false;
  loading: boolean = false;

  constructor(private service: ServiceFileService, private _fb: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.urlParameter = this.route.snapshot.paramMap.get('name');

    if (localStorage.getItem("isAuthenticated") && localStorage.getItem("isAuthenticated") == "Yes") {
      this.router.navigate(['/all-categories/mindful'])

    }
    window.scrollTo(0, 0);
    this.formGroup = this._fb.group({
      'name': ['', [Validators.required, Validators.minLength(3)]],
      'last_name': ['', [Validators.required, Validators.minLength(3)]],
      'phoneNumber': ['', [Validators.required, Validators.minLength(10)]],
      'email': ['', [Validators.required, Validators.minLength(6), Validators.pattern(/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
      'password': ['', [Validators.required, Validators.minLength(6)]],
      'confirm_password': ['', Validators.required],
    }, {
      validator: this.ConfirmedValidator('password', 'confirm_password')
    })

  }

  ConfirmedValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }


    }
  }


  ConfirmedPasswordValidator(DATA: any) {
    return DATA.confirmedValidator;
  }

  validateAllFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFields(control);
      }
    });
  }

  formvalidfn(e: any) {
    const $this = this;
    if ($(e.target).closest('form').find('div').hasClass('alert')) {
      $this.formvalid = false;
    } else {
      $this.formvalid = true;
    }


  }

  addUsers(event: any) {
    const $this = this;
    this.error = false;
    this.errorthrown = "";
    this.validateAllFields(this.formGroup);
    setTimeout(() => {
      $this.formvalidfn(event);

      if ($this.formvalid) {
        this.loading = true;

        $this.disablebtn = true;
        var data = {};
        data = {
          name: $this.name,
          last_name: $this.last_name,
          email: $this.email,
          phone: $this.phoneNumber,
          password: $this.password
        }
        var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!filter.test($this.email)) {
          $this.error = true;
          $this.disablebtn = false;

        } else {
          $this.service.register(data).subscribe(
            (response: any) => {
              if (response.error == 1) {
                $this.errorthrown = response.data.message;
              } else {
                Swal.fire({
                  title: 'You are registered successfully',
                  text: 'Thanks!',
                  icon: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok',
                  cancelButtonText: 'Cancel'
                }).then((result) => {

                  if ($this.urlParameter == "mindful") {
                    $this.submitLoginForm($this.urlParameter);
                  } else if ($this.urlParameter == "neurodynamic") {
                    $this.submitLoginForm($this.urlParameter);
                  } else if ($this.urlParameter == "rooted") {
                    $this.submitLoginForm($this.urlParameter);
                  } else {
                    $this.submitLoginForm('');
                  }

                })
                
              }

              $this.disablebtn = false;
              // $this.router.navigate(['/login'])
              // var res = response;
              // if (res.error == 1) {
              //   $this.messageError = res.message;
              // } else {
              //   $this.messageError = "";
              //   // $this.signupListing();
              //   $this.usersArray = response.data;
              //   // alert('res');
              //   console.log(data)
              // }
              this.loading = false;

            }, (err: any) => {
              console.log(err)
              $this.disablebtn = false;

            }
          )
        }
      }

    }, 1000);

  }

  signupListing() {
    throw new Error('Method not implemented.');
  }

  allowAlphabetic(e: any) {
    var regex = new RegExp(/^[a-zA-Z]+$/);
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      return true;
    }
    else {
      e.preventDefault();
      return false;
    }
  }


  submitLoginForm(slug: any) {
    let data = {                        //let temporary variable  //data object
      email: this.email,
      password: this.password,
    }

    this.service.login(data).subscribe(
      (response: any) => {

        var res = response;
        if (res.error == 1) {
          this.messageError = res.message;
        } else {
          this.messageError = "";
          localStorage.setItem("ID", res.data['id'])
          localStorage.setItem("role", res.data['role'])
          localStorage.setItem("token", res.data['token'])
          localStorage.setItem("image", res.data['image'])
          localStorage.setItem("isAuthenticated", "Yes")
          localStorage.setItem("name", res.data['name'])
          localStorage.setItem("remember_me", this.remember_me);

          if (this.remember_me) {
            localStorage.setItem("uiD", window.btoa(res.data['id']));
            localStorage.setItem("ecr", window.btoa(res.data['email']));
            localStorage.setItem("pcr", window.btoa(this.password));

          }

          if (slug == "mindful") {
            this.router.navigate(['/all-categories', 'mindful'])
          } else if (slug == "neurodynamic") {
            this.router.navigate(['/all-categories', 'neurodynamic'])
          } else if (slug == "rooted") {
            this.router.navigate(['/all-categories', 'rooted'])
          } else {
            this.router.navigate(['/'])
          }

        }
      }, err => {
        console.log(err)
      }
    )
  }

}
