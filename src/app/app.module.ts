import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatPaginatorModule} from '@angular/material/paginator';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesComponent } from './categories/categories.component';
import { LoginComponent } from './auth/login/login.component';
import { LoginComponent as adminLogin} from './admin/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeachersComponent } from './teachers/teachers.component';
import { ClassesComponent } from './classes/classes.component';

import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';             
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CalendarModule} from 'primeng/calendar';
import { BookingsComponent } from './bookings/bookings.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './auth/register/register.component';
import { MyBookingsComponent } from './my-bookings/my-bookings.component';
import { FooterComponent } from './footer/footer.component';
import { IndexComponent } from './index/index.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ScholarshipComponent } from './scholarship/scholarship.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ProfileComponent } from './profile/profile.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { AdminheaderComponent } from './admin/adminheader/adminheader.component';
import { AdminfooterComponent } from './admin/adminfooter/adminfooter.component';
import { AdmincategoriesComponent } from './admin/admincategories/admincategories.component';
import { AdminclassesComponent } from './admin/adminclasses/adminclasses.component';
import { SidebarComponent } from './admin/sidebar/sidebar.component';
import { MymusicComponent } from './admin/mymusic/mymusic.component';
import { ManageBookingsComponent } from './admin/manage-bookings/manage-bookings.component';
import { TutorsComponent } from './admin/tutors/tutors.component';
import { ManagePaymentsComponent } from './admin/manage-payments/manage-payments.component';
import { ToastrModule } from 'ngx-toastr';
import { MindfulComponent } from './mindful/mindful.component';
import { NeurodynamicMusicTherapyComponent } from './neurodynamic-music-therapy/neurodynamic-music-therapy.component';
import { RootedPrivateLessonsComponent } from './rooted-private-lessons/rooted-private-lessons.component';
import { AllCategoriesComponent } from './all-categories/all-categories.component';
import { PaymentComponent } from './payment/payment.component';
import { MindfulMusiciansComponent } from './mindful-musicians/mindful-musicians.component';
import { AdminForgotPasswordComponent } from  './admin/forgot-password/forgot-password.component';
import { UsersComponent } from './admin/users/users.component';
import { SubcategoriesComponent } from './admin/subcategories/subcategories.component';
import { DonationComponent } from './donation/donation.component';
import { MyPaymentsComponent } from './my-payments/my-payments.component';
import { MySavecardsComponent } from './my-savecards/my-savecards.component';
import { SiteloaderComponent } from './siteloader/siteloader.component';
import { InquiryComponent } from './inquiry/inquiry.component';


@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    LoginComponent,
    TeachersComponent,
    ClassesComponent,
    BookingsComponent,
    HomeComponent,
    HeaderComponent,
    RegisterComponent,
    MyBookingsComponent,
    FooterComponent,
    IndexComponent,
    AboutUsComponent,
    ScholarshipComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    AdminForgotPasswordComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ResetPasswordComponent,
    ContactUsComponent,
    adminLogin,
    AdmindashboardComponent,
    AdminheaderComponent,
    AdminfooterComponent,
    AdmincategoriesComponent,
    AdminclassesComponent,
    SidebarComponent,
    MymusicComponent,
    ManageBookingsComponent,
    TutorsComponent,
    ManagePaymentsComponent,
    MindfulComponent,
    NeurodynamicMusicTherapyComponent,
    RootedPrivateLessonsComponent,
    AllCategoriesComponent,
    PaymentComponent,
    MindfulMusiciansComponent,
    UsersComponent,
    SubcategoriesComponent,
    DonationComponent,
    MyPaymentsComponent,
    MySavecardsComponent,
    SiteloaderComponent,
    InquiryComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AccordionModule,
    CalendarModule,
    MatPaginatorModule,
    ToastrModule.forRoot({
      positionClass :'toast-top-right'
    })
    // StripeModule.forRoot("...YOUR-STRIPE-KEY-HERE...")
    
    
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }