import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import * as $ from 'jquery';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-inquiry',
	templateUrl: './inquiry.component.html',
	styleUrls: ['./inquiry.component.css']
})
export class InquiryComponent implements OnInit {

	stripe: any;
	elements: any;
	card: any = [];
	cdate = new Date();
	noofbookingBool = false;
	activePost: any = [];
	totatcartprice: any = "";
	formProcess: any = '';
	customStripeForm: any;
	submitted: any;
	//error: string="";
	bookingCount: any = 0
	number: any = ""
	expiry_month: any = ""
	expiry_year: any = ""
	cvc: any = "";
	param1: any = "";
	Booking_cartArray: any = [];
	uname: any = "";
	parentname: any = "";
	childname: any = "";
	email: any = "";
	age: any = "";
	instrument: any = "";
	expiryMonth: any = "";
	expiryYear: any = "";
	cvvNumber: any = "";
	securityCode: any = "";
	price: any = "";
	priceError: any = "";
	myGroup = new FormGroup({});
	error: boolean = false;
	mainurl:any = environment.PROJECT_URL;
	backendurl: any = this.mainurl+"/centrallyRooted/public/";
	constructor(private _fb: FormBuilder, private router: Router, private service: ServiceFileService, private route: ActivatedRoute) { }

	ngOnInit(): void {

		this.myGroup = this._fb.group({
			'parentname': ['', [Validators.required]],
			'childname': ['', [Validators.required]],
			'email': ['', [Validators.required, Validators.minLength(6), Validators.pattern(/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
			'age': ['', [Validators.required]],
			'instrument': ['', [Validators.required]],

		})


		this.route.queryParams.subscribe(params => {
			this.param1 = params['cat'];
			// this.param2 = params['param2'];

		});


		this.scriptforall();



	}


	inquiry_send(e: any) {
		var $this = this;
		$(e.target).closest('form').find('div.rerror').show();
		$this.validateAllFields(this.myGroup);

		if (this.myGroup.valid) {

			$('.full_page_loader_app').show();
			const data = {
				parentname: this.parentname,
				email: this.email,
				childname: this.childname,
				age: this.age,
				instrument: this.instrument
			}
			this.service.inquiry_send(data).subscribe(
				(response: any) => {
					$('.full_page_loader_app').hide();

					if (response.error == 0) {
						Swal.fire({
							title: 'Success',
							text: "Thank you for your inquiry! We will respond within 48 hours with an email!",
							icon: "success"
						}).then(function () {
							$('.inquiryform .close').trigger('click');
							$('.inquiryform form').trigger('reset');

						});
					} else {
						Swal.fire({
							title: 'Error',
							text: 'Oops something went wrong!',
							icon: 'warning',
						})
					}

				},
				err => {
					console.log(err)
				}
			);


		}

	}




	validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
			const control = myGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}





	scriptforall() {
		const $this = this;

		$(window).click(function (event: any) {
			setTimeout(() => {


				var modal = document.getElementById("donationnow");
				if (event.target == modal) {
					$('#donationnow form').trigger("reset");

					$('.stripe_error').removeClass('alert alert-danger').text('');

				}

				var modal2 = document.getElementById("add-payment");
				if (event.target == modal2) {
					$('#add-payment form').trigger("reset");

					$('.stripe_error').removeClass('alert alert-danger').text('');

				}
			}, 1000);

		});

		$('.close,.close-btn').click(function (event: any) {
			let cthis = $(this);
			setTimeout(() => {

				cthis.closest('#donationnow').find('form').trigger("reset");

				$('.stripe_error').removeClass('alert alert-danger').text('');

			}, 1000);

		});



	}

	numberOnly(event: any): boolean {
		const charCode = (event.which) ? event.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		const element = event.target;
		this.formatCardNumber(element);
		return true;

	}


	formatCardNumber(element: any) {

		if (element.value.length > 14) {
			var position = element.selectionStart;
			element.value = element.value.replace(/\W/gi, '').replace(/^(.{4})(.{4})(.{4})(.*)$/, "$1 $2 $3 $4");
			if (element.value.length != 19) {
				element.setSelectionRange(position, position);
			}
		}
		else {
			element.value = element.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
		}

	}


	allowNumeric(e: any) {

		var charCode = (e.which) ? e.which : e.keyCode

		if (String.fromCharCode(charCode).match(/[^0-9]/g)) {

			e.preventDefault();
		}

	}

	allowAlphabetic(e: any) {
		var regex = new RegExp(/^[a-zA-Z\s]+$/);
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}
		else {
			e.preventDefault();
			return false;
		}
	}


}
