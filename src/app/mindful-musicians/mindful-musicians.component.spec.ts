import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MindfulMusiciansComponent } from './mindful-musicians.component';

describe('MindfulMusiciansComponent', () => {
  let component: MindfulMusiciansComponent;
  let fixture: ComponentFixture<MindfulMusiciansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MindfulMusiciansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MindfulMusiciansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
