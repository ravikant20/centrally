
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-mindful-musicians',
  templateUrl: './mindful-musicians.component.html',
  styleUrls: ['./mindful-musicians.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class MindfulMusiciansComponent implements OnInit {
  dates: Date[] = [new Date()];
  value: Date = new Date();
  loginUser: any = false;
  baseurls:any = environment.adminbaseUrl;
  mainurl:any = environment.PROJECT_URL;
  assetpath:any = this.mainurl+'/assets';


  constructor(private router: Router) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.scriptforAll();
    if (localStorage.getItem("isAuthenticated") && localStorage.getItem("isAuthenticated") == "Yes") {
      this.loginUser = true;

    }
  }

  scriptforAll() {


    $(document).ready(function () {
      // // Toggle plus minus icon on show hide of collapse element
      $(document).on('click', '.card-header', function (e) {
        e.preventDefault();
        $(".collapse").removeClass('show');
        $('.card-header').find('button i.fa').removeClass('fa-minus').addClass('fa-plus');
        if ($(this).find('button').hasClass('collapsed')) {
          $(this).find('button i.fa').removeClass('fa-minus').addClass('fa-plus');
          $(this).closest('.card').find('.collapse').removeClass('show');

        } else {
          $(this).find('button i.fa').removeClass('fa-plus').addClass('fa-minus');
          $(this).closest('.card').find('.collapse').addClass('show');
        }
      });

    });

  }

  checkForRegisterMindful(value: any) {
    var isAuthenticated = localStorage.getItem('isAuthenticated');
    if (isAuthenticated) {
      this.router.navigate(['/all-categories', value])
    } else {
      this.router.navigate(['/register', value])
    }

  }


  downloadMusic(fileurl: any, filename: any) {
    const data = {
      fileurl: fileurl
    }
    const fileURL = fileurl;
    var a = document.createElement('a');
    a.href = fileURL;
    a.download = filename;
    a.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }, 100);

  }


}
