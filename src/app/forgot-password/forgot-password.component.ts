import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgetPassword:any = "";
  error:any = "";
  email:any = "";
  ShowSubmitLoader:boolean = false
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

  

  submitchangePassword(){
    this.ShowSubmitLoader= true;
    this.error = "";
    var data  = {
      email:this.email
    }    

    this.service.forgotPassword(data).subscribe(
      (response :any) => {
        this.ShowSubmitLoader= false;
        if(response['error'] == 1){
          this.error = response['message'];
        }else{
          Swal.fire({
            title: 'Success',
            text: 'Please check your mail to reset password',
            icon: 'success',
            confirmButtonText: 'OK'
          })
          this.email = ""          
        }

      })
  }

}
