import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  new_password:any = "";
  new_confirm_password:any = "";
  error:any=[];
  param1: string = "";
  param2: string = "";
  ShowSubmitLoader:boolean = false;
  email:any = "";
  formGroup= new FormGroup({});

  constructor(private service: ServiceFileService,private _fb: FormBuilder,private route: ActivatedRoute, private router: Router) { 
    this.route.queryParams.subscribe(params => {
      this.param1 = params['uid'];

    });
    // console.log(this.param1)
  }

  ngOnInit(): void {
    // alert(localStorage.getItem('token'))
    if(localStorage.getItem('token')){
      this.router.navigate(['/'])
    }
    window.scrollTo(0, 0);
    this.resetpassordlink();
    this.formGroup = this._fb.group({
     
      'new_confirm_password': ['',[Validators.required,Validators.minLength(6)]],
      'new_password': ['',[Validators.required,Validators.minLength(6)]]
      

   })


 }

 validateAllFields(formGroup: FormGroup){         
   Object.keys(formGroup.controls).forEach(field => {  
       const control = formGroup.get(field);            
       if (control instanceof FormControl) {             
           control.markAsTouched({ onlySelf: true });
       } else if (control instanceof FormGroup) {        
           this.validateAllFields(control);  
       }
   });
 }

  changePassword(){
    this.ShowSubmitLoader = true;
    this.error = "";
    if (this.formGroup.valid) {
      
    } else {
        this.validateAllFields(this.formGroup); 
        return;
    }
    var data  = {
      new_password:this.new_password,
      new_confirm_password:this.new_confirm_password,
      uid:this.param1,
      email:this.email
    }    

    this.service.resetPassword(data).subscribe(
      (response :any) => {
        this.ShowSubmitLoader = false
        if(response['error'] == 1){
          this.error = "Password not matched or is invalid!";
        }
        else{
          Swal.fire({
            title: 'Success',
            text: 'Your password has been successfully changed',
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
          this.router.navigate(['/login'])
            }
        })
        }
        
        // 

      })
  }


  resetpassordlink(){
    const data = {
      uid :  this.param1
    }
    this.service.resetPasswordlink(data).subscribe(
      (response :any) => {
        if(response['linkexpired'] == true){
          Swal.fire({
            text: 'Reset Password link is expired! Please request a new one.',
            icon: 'warning',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
          this.router.navigate(['/login'])
            }
        })
        this.router.navigate(['/login'])
        }else if(response['error'] == 0){
          if(response['data']){
          this.email = response['data'].email;
        }
        }
        

      })

  }
}
