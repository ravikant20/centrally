import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-payments',
  templateUrl: './my-payments.component.html',
  styleUrls: ['./my-payments.component.css']
})
export class MyPaymentsComponent implements OnInit {

  bookingArray:any = []
  mainurl:any = environment.PROJECT_URL;
  constructor(private service: ServiceFileService,private router: Router) { }

  ngOnInit(): void {
    this.getMyBookngs()

  }

  getMyBookngs() {
    this.service.myBookings().subscribe(
      (response: any) => {
       console.log(response);
       this.bookingArray = response.message;
      },
      err => {
        console.log("err", err)
      }
    );
  }
}
