import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
declare var Stripe: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  classArray: any = [];
  stripe: any;
  elements: any;
  card: any =[];
  noofbookingBool= false;
  activePost:any =[];
  error: string="";
  bookingCount:any = 0
  number:any=""
  expiry_month:any =""
  expiry_year:any =""
  cvc:any = "";
  constructor(private service: ServiceFileService,private cdr: ChangeDetectorRef,private router: Router) { }

  ngOnInit(): void {
    this.getActiveClasses()
  }

  getActiveClasses() {
    this.service.getClasses().subscribe(
      (response: any) => {
        var arrayList = response.message
        // console.log("this.categoriesArray ", arrayList)
        for (var i = 0; i < arrayList.length; i++) {
          if (arrayList[i].bookings) {
            if (arrayList[i].class_type == 1) {
              // alert(parseInt(this.classArray[i].capacity) +" "+this.classArray[i].bookings.length)
              if (parseInt(arrayList[i].capacity) <= arrayList[i].bookings.length) {
                delete arrayList[i];
              }else{
                this.classArray.push(arrayList[i])
              }
            } else {
              if (arrayList[i].bookings.length > 0) {
                delete arrayList[i];
              }else{
                this.classArray.push(arrayList[i])    
              }
            }
          }else{
            this.classArray.push(arrayList[i])
          }

        }
        // this.classArray = arrayList;
        
      },
      err => {
        console.log("err", err)
      }
    );
  }



  submitPayment(){

    // console.log("this.card",this.card)
    this.card ={
      bookingCount:this.bookingCount,
      number:this.number,
      expiry_month:this.expiry_month,
      expiry_year:this.expiry_year,
      cvc:this.cvc
    }
    const data = {
      card:this.card,
      class:this.activePost,
    }
    this.service.makePayment(data).subscribe(
      (response: any) => {
        
      },
      err => {
        console.log("err", err)
      }
    );
  }

  activeBook(id:any,post:any){
    // console.log(post)
    this.activePost = post;
    if(post.class_type ==2){
      this.noofbookingBool = true;
    }else{
      this.noofbookingBool = false;
    }

    if(localStorage.getItem('role') == '2'){
      if(document.getElementById("openModel") != null) {
        (document.getElementById("openModel") as HTMLElement).click();
      }
    }else{
      this.router.navigate(['/'])
    }
      
    
    // document.getElementById("").click()
  }
}
