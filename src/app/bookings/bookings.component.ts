import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  bookingArray:any = [];
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    this.getBookings()
  }

  getBookings() {
    this.service.getBookings().subscribe(
      (response: any) => {
        this.bookingArray = response.message;
        console.log("this.categoriesArray ", this.bookingArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

  
  updateData(id:any){
    var data = {
      id:id
    }
    this.service.updateBookings(data).subscribe(
      (response: any) => {
        this.getBookings();
        // this.bookingArray = response.message;
        // console.log("this.categoriesArray ", this.bookingArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

}
