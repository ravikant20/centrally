import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class AdminForgotPasswordComponent implements OnInit {

  messageError: any;
  forgetPassword:any = "";
  error:any = "";
  email:any = "";
  ShowSubmitLoader:boolean = false

  constructor(private service: ServiceFileService, private router: Router) { }

  ngOnInit(): void {
  }
  
  submitForgotPasswordForm(){
    this.ShowSubmitLoader= true;
    this.error = "";
    var data  = {
      email:this.email
    }  
    
   // console.log(data);

    this.service.forgotPassword(data).subscribe(
      (response :any) => {
        this.ShowSubmitLoader= false;
        if(response['error'] == 1){
          this.error = response['message'];
        }else{
          Swal.fire({
            title: 'Success',
            text: 'Please check your mail to reset password',
            icon: 'success',
            confirmButtonText: 'OK'
          })
          this.email = ""          
        }
      })

  }

}
