import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';


@Component({
	selector: 'app-mymusic',
	templateUrl: './mymusic.component.html',
	styleUrls: ['./mymusic.component.css'],
	encapsulation: ViewEncapsulation.None,
})
export class MymusicComponent implements OnInit {
	value: Date = new Date();
	date1: Date = new Date();
	totalmusicArray: any = [];
	dates: Date[] = [new Date()];
	classesDatesArray = [];
	selectedDate: any = [];
	todaysSessions: any = [];
	musicArray: any = [];
	todayDay: any = "";
	toDay: any = "";
	fileChanges: boolean = false;
	musicImage: any = []
	imageSrc: string | ArrayBuffer | any = "";
	name: any = "";
	musicId:any = "";
	cdate = new Date();
	cd: any = this.cdate.getUTCDate();
	cm: any = this.cdate.getUTCMonth();
	cy: any = this.cdate.getFullYear();
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];
	baseurls = environment.adminbaseUrl;
	length: any = 10;
	pageSize: any = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	Index: any = 1;
	constructor(private service: ServiceFileService) { }

	ngOnInit(): void {
		$('.full_page_loader_app').show();
		$('.dashboard-table-outer').css('opacity', 0).hide();
		this.scriptforAll();
		this.getMusic();
		// this.getTodaysSession();
	}

	getClassesDates() {
		const $this = this;
		this.service.getClassesDates().subscribe(
			(response: any) => {
				this.classesDatesArray = response.message;
				this.dates = [];
				var newarray: any = [];
				var fulldatenewarray: any = [];
				for (var i = 0; i < this.classesDatesArray.length; i++) {
					if (this.classesDatesArray[i] < this.fullformattodate(this.date1)) {
						this.dates.push(new Date(this.classesDatesArray[i]));

						var startdate = this.classesDatesArray[i];
						let fulldate = new Date(startdate);
						fulldatenewarray.push(fulldate);
						let y = fulldate.getFullYear();
						let d = fulldate.getUTCDate();
						let m = $this.monthNames[fulldate.getUTCMonth()];
						let cnewarray = { 'y': y, 'm': m, 'd': d };
						newarray.push(cnewarray);

					}
				}






				console.log(this.dates)
			},
			err => {
				console.log("err", err)
			}
		);
	}


	selectDate(event: any) {


		var today = new Date(event);
		var daysss = String(today.getUTCMonth() + 1)
		if (daysss.toString().length == 1) {
			daysss = "0" + daysss;
		}
		this.toDay = today;
		this.todayDay = today.getFullYear() + '-' + daysss + '-' + today.getUTCDate();

		this.getTodaysSession(today);

	}

	fullformattodate(date: any) {
		var today = date;
		var daysss = String(today.getUTCMonth() + 1)
		if (daysss.toString().length == 1) {
			daysss = "0" + daysss;
		}
		return today.getFullYear() + '-' + daysss + '-' + today.getUTCDate();

	}

	// getMultipleDate(values: any){

	//   this.selectedDate =  values;
	//   console.log(this.selectedDate);
	// }

	getTodaysSession(today: any) {
		// var today = new Date();
		var month = String(today.getUTCMonth() + 1);
		if (month.toString().length == 1) {
			month = "0" + month;
		}
		var today_date = today.getFullYear() + '-' + (month) + '-' + today.getUTCDate();
		// alert(today_date)
		var data = {
			tdate: today_date
		}

		this.service.getTodaysSessions(data).subscribe(
			(response: any) => {
				this.todaysSessions = response.message
				console.log("this.todaysSessions", this.todaysSessions)
				// console.log("response",response)
			},
			err => {
				console.log("err", err)
			}
		);

	}



	onChange(event: any) {
		this.onFileChange(event, event.target.files);
	}


	onChangeMusicnameInput(event:any){
		event.target.value= "";
	}
	

	private onFileChange(event: any, files: File[]) {
		const $this = this;
		const file = event.target.files[0];
		const reader = new FileReader();
		reader.onload = e => this.imageSrc = reader.result;
		this.musicImage = file;
		reader.readAsDataURL(file);
		setTimeout(function () {
			$(event.target).closest('.upload-image-music').find('img').attr('src', $this.imageSrc);
		}, 100);

	}

	getMusic() {


		this.service.getMusic().subscribe(
			(response: any) => {
				this.totalmusicArray = response.data;
				this.musicArray = response.data.slice(0, 10);
				$('.full_page_loader_app').hide();
				$('.dashboard-table-outer').css('opacity', 1).show();
			},
			err => {
				console.log("err", err)
			}
		);

	}

	fileUploadMusicInput(event:any){
		event.target.value= "";
	}

	fileUpload(event: any) {
		const $this = this;
	
		$this.musicId = ($(event.target).closest('tr').find('.musicid').val()?$(event.target).closest('tr').find('.musicid').val():'');
		
		if (event.target.files && event.target.files[0]) {
			$(event.target).closest('li').find('.upload_music_loader_app').show();
			$(event.target).closest('tr').find('.addclonemusic').attr('disabled', 'disabled');
			$(event.target).attr('disabled', 'disabled');
			const file = event.target.files[0];
			const reader = new FileReader();

			const formdata = new FormData();
			formdata.append("file", (event.target.files[0] ? event.target.files[0] : $(event.target).closest('tr').find('.musichiddenfield').val()));
			formdata.append("id", ($this.musicId ? $this.musicId:''));
			formdata.append('image', ($this.musicImage ? $this.musicImage : $(event.target).closest('tr').find('.musicimage').val() ? $(event.target).closest('tr').find('.musicimage').val() : ''));
			formdata.append('name', ($this.name ? $this.name : $(event.target).closest('tr').find('.musicname').val() ? $(event.target).closest('tr').find('.musicname').val() : ''));
			reader.readAsDataURL(file);
			$this.service.uploadMusic(formdata).subscribe(
				(response: any) => {
					$this.getMusic();
					$this.name = "";
					$('.upload-image-music.deft').find('img').attr('src', "assets/images/week-img.png");
					$(event.target).closest('li').find('.upload_music_loader_app').hide();
					$(event.target).closest('tr').find('.addclonemusic').removeAttr('disabled');
					$(event.target).removeAttr('disabled');
					
				},
				err => {
					console.log("err", err)
				}
			);
		}
	
	}

	removeMusic(event: any, id: any, removerow: any) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			confirmButtonColor: '#738c72',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.isConfirmed) {
				const formdata = {
					id: id,
					removerow: removerow
				}
				this.service.removeMusic(formdata).subscribe(
					(response: any) => {
						this.getMusic();

					},
					err => {
						console.log("err", err)
					}
				);
			}
		});

	}

	scriptforAll() {
		const $this = this;
	
	}


	uploadMusicValidation(event:any){
		$('table tr td.music-td').each(function(){
			if($(this).find('.musichiddenfield').val()){

			}else{
				$(this).find('.error.alert').text('Music is required');

			}
		});

		$('table tr td.music-name-td').each(function(){
			if($(this).find('.musicname').val()){

			}else{
				$(this).find('.error.alert').text('Music Name is required');

			}
		});
		

	}


	validationMusicName(event:any){
		if ($(event.target).closest('td').prev('td').find('.musicname').val()) {
			$(event.target).closest('td').prev('td').find('.error.alert').text('');

		} else {
			event.preventDefault();
			$(event.target).closest('td').prev('td').find('.error.alert').text('Music Name is required');

		}
	}

	OnPageEvent(event: PageEvent) {
		const startIndex = event.pageIndex * event.pageSize;
		let endIndex = startIndex + event.pageSize;
		if (endIndex > this.totalmusicArray.length) {
			endIndex = this.totalmusicArray.length;
		}
		this.Index = startIndex + 1;
		this.musicArray = this.totalmusicArray.slice(startIndex, endIndex);
	}



}
