import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = "";
  password: string = "";
  remember_me:any = ""; 
  messageError: any;
  constructor(private service: ServiceFileService, private router: Router) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    if(localStorage.getItem("isAuthenticated") && localStorage.getItem("isAuthenticated")== "Yes"){
      this.router.navigate(['/admin/dashboard']);

    }
    // localStorage.removeItem("role")
    // localStorage.removeItem("type" )
    // localStorage.removeItem("token" )

    this.remember_me = localStorage.getItem("remember_me");
    const ecr =  localStorage.getItem("ecr");
    const pcr = localStorage.getItem("pcr");
     localStorage.getItem("pcr");
     this.email = (ecr?window.atob(ecr):'');
     this.password = (pcr?window.atob(pcr):'');
   if(this.remember_me == "true"){
     this.remember_me = true;
     
   }else{
    this.remember_me = false;


   }
  }
  submitLoginForm() {
    // alert("Sdf")
    let data = {                        //let temporary variable  //data object
      email: this.email,
      password: this.password,
    }

    this.service.login(data).subscribe(
      (response: any) => {
        // console.log(response.errors)
        var res = response;
        if (res.error == 1) {
          this.messageError = res.message;
        } else {
          this.messageError = "";
          if (res.data['role'] == 1) {
            localStorage.setItem("role", res.data['role'])
            localStorage.setItem("type", "admin")
            localStorage.setItem("token", res.data['token'])
            //localStorage.setItem('admin_id',res.data['id'])
            this.router.navigate(['admin/dashboard'])

            localStorage.setItem("ID",res.data['id'])
          localStorage.setItem("image", res.data['image'])
          localStorage.setItem("isAuthenticated","Yes")
          localStorage.setItem("name", res.data['name'])
          localStorage.setItem("remember_me",this.remember_me);

          if(this.remember_me){
            localStorage.setItem("uiD",window.btoa(res.data['id']));
            localStorage.setItem("ecr",window.btoa(res.data['email']));
            localStorage.setItem("pcr",window.btoa(this.password));

          }

          } else {
            this.messageError = "Please Enter valid credentials!" 
          }
        }
      }, err => {
        console.log(err)
      }
    )
  }

  remembercheckCheckBoxvalue(event:any){
    this.remember_me = event.target.checked;
  }

}
