import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-tutors',
  templateUrl: './tutors.component.html',
  styleUrls: ['./tutors.component.css']
})
export class TutorsComponent implements OnInit {
  TeachersArray: any = [];
  totalTeachersArray: any = [];
  teacherName = "";
  phoneNumber = "";
  city = "";
  state = "";
  activeId = "";
  zipCode = "";
  streetAddress = "";
  updateOrInsert: boolean = false;
  searchText: string = "";
  searchArray: any = [];
  myGroup = new FormGroup({});
  norecordfound: any = false;
  submitdisable: any = false;
  length: any = 10;
  pageSize: any = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  Index: any = 1;
  constructor(private _fb: FormBuilder, private service: ServiceFileService) { }

  ngOnInit(): void {
    $('.full_page_loader_app').show();
    $('.dashboard-table-outer').css('opacity', 0).hide();
    this.myGroup = this._fb.group({
      'teacherName': ['', [Validators.required]],
      'phoneNumber': ['', [Validators.required]],
      'city': ['', [Validators.required]],
      'state': ['', [Validators.required]],
      'zipCode': ['', [Validators.required]],
      'streetAddress': ['', [Validators.required]]

    });
    this.getTeachers()
  }


  validateAllFields(myGroup: FormGroup) {
    Object.keys(myGroup.controls).forEach(field => {
      const control = myGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFields(control);
      }
    });
  }

  getTeachers() {
    this.service.getTeachers().subscribe(
      (response: any) => {
        this.totalTeachersArray = response.message;
        this.TeachersArray = response.message.slice(0, 10);

        if (this.TeachersArray.length == 0) {
          this.norecordfound = true;
        }else{
          this.norecordfound = false;

        }
        $('.full_page_loader_app').hide();
        $('.dashboard-table-outer').css('opacity', 1).show();

      },
      err => {
        console.log("err", err)
      }
    );
  }


  saveTeacher(e: any) {

    this.validateAllFields(this.myGroup);





    if (this.myGroup.valid) {
      this.submitdisable = true;

      var data = {
        name: this.teacherName,
        phone_number: this.phoneNumber,
        city: this.city,
        state: this.state,
        zip_code: this.zipCode,
        street_address: this.streetAddress,
        status: '1'
      }


      if (!this.updateOrInsert) {
        this.service.addTeachers(data).subscribe(
          (response: any) => {

            this.getTeachers()

            var close = document.getElementById('add-tutor');
            if (close) {
              close.click()
            }
            $('button.close').trigger('click');


          },
          err => {
            console.log("err", err)
            this.submitdisable = false;

          }
        );
      } else {

        var daa = {
          name: this.teacherName,
          phone_number: this.phoneNumber,
          city: this.city,
          state: this.state,
          zip_code: this.zipCode,
          street_address: this.streetAddress,
          status: '1',
          id: this.activeId
        }




        this.service.updateTeachers(daa).subscribe(
          (response: any) => {

            this.updateOrInsert = false;
            this.getTeachers()
            var close = document.getElementById('add-tutor');
            if (close) {
              close.click()
            }
            $('button.close').trigger('click');

          },
          err => {
            console.log("err", err)
            this.submitdisable = false;

          }
        );
      }
    }

  }

  addTutor(event: any) {
    $('#add-tutor').find('form').trigger('reset');
   document.querySelectorAll('#add-tutor .modal-title')[0].innerHTML = "Add Tutors";
   document.querySelectorAll('#add-tutor button.continue')[0].innerHTML = "Save";
    this.submitdisable = false;
    this.teacherName = "";
    this.phoneNumber = "";
    this.city = "";
    this.state = "";
    this.zipCode = "";
    this.streetAddress = "";
    this.updateOrInsert = false;
    this.activeId = "";

  }

  updateData(name: any, phone: any, city: any, state: any, zip_code: any, street_address: any, id: any) {
    document.querySelectorAll('#add-tutor .modal-title')[0].innerHTML = "Edit Tutors";
    document.querySelectorAll('#add-tutor button.continue')[0].innerHTML = "Update";
    this.submitdisable = false;
    this.teacherName = name;
    this.phoneNumber = phone;
    this.city = city;
    this.state = state;
    this.zipCode = zip_code;
    this.streetAddress = street_address;
    this.updateOrInsert = true;
    this.activeId = id;
  }





  changeStatus(post: any, event: any) {
    console.log(event.srcElement.checked)
    var status = 0;
    if (event.srcElement.checked) {
      if (confirm('Are you sure you want to Active this teacher')) {

        status = 1;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    else {
      if (confirm('Are you sure you want to inctive this teacher')) {
        status = 0;
      } else {
        event.srcElement.checked = !event.srcElement.checked
      }
    }
    var daa = {
      id: post.id,
      status: status
    }

    this.service.updateTeachersStatus(daa).subscribe(
      (response: any) => {
        this.getTeachers();
      },
      err => {
        console.log("err", err)
      }
    );
  }


  deleteTeacher(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      confirmButtonColor: '#738c72',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteTeachers({ id: id }).subscribe(
          (response: any) => {
            this.getTeachers()
          },
          err => {
            console.log("err", err)
          }
        );
      }
    });
  }

  searchTeachers(e: any) {

    let data = {
      searchValue: this.searchText
    }

    this.service.searchTeachers(data).subscribe(
      (response: any) => {
        this.totalTeachersArray = response.message;
        this.TeachersArray = response.message.slice(0, 10);
        // console.log("this.TeachersArray", this.TeachersArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }


  // tutor_name:any;
  // phone:any;
  // city:any;
  // state:any;
  // zipcode:any;
  // street_address:any;
  // error:boolean = false;
  // formGroup= new FormGroup({});

  // constructor(
  //   private service: ServiceFileService,
  //   private router: Router,
  //   private _formBuilder:FormBuilder,
  //   private toaster:ToastrService
  // ) { }

  // ngOnInit(): void {

  // this.formGroup = new FormGroup({
  //   tutor_name : new FormControl('',[Validators.required]),
  //   phone      : new FormControl('',[Validators.required,Validators.pattern('')]),
  //   city       : new FormControl('',[Validators.required]),
  //   state      : new FormControl('',[Validators.required]),
  //   zip_code   : new FormControl('',[Validators.required,Validators.maxLength(6)]),
  //   street_address : new FormControl('',[Validators.required])
  // });
  // }

  // addTutors(){
  //   let data = {
  //     tutor_name :this.formGroup.value.tutor_name,
  //     phone:this.formGroup.value.phone,
  //     city:this.formGroup.value.city,
  //     state:this.formGroup.value.state,
  //     zipcode:this.formGroup.value.zip_code,
  //     street_address:this.formGroup.value.street_address
  //   }

  //   this.service.addTutors(data).subscribe(
  //     result=>{
  //       var res:any = result;
  //       if(res.status=='1'){
  //         this.toaster.success(res.message,'Success');
  //       }else{
  //         this.toaster.error(res.error,'Error');     
  //       }
  //     }
  //   )
  // }

  // get validatedForm(){
  //   return this.formGroup.controls;
  // }

  OnPageEvent(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.totalTeachersArray.length) {
      endIndex = this.totalTeachersArray.length;
    }
    this.Index = startIndex + 1;
    this.TeachersArray = this.totalTeachersArray.slice(startIndex, endIndex);
  }

}
