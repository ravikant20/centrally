import { Component, OnInit } from '@angular/core';
import {ServiceFileService } from 'src/service-file.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  UsersArray:any=[];
  baseurls = environment.uploadsImageUrl;
  searchText:any="";
  norecordfound:any = false;
  totalUsersArray:any=[];
  length: any = 10;
  pageSize: any = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  Index: any = 1;
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
     $('.full_page_loader_app').show();
		$('.dashboard-table-outer').css('opacity',0).hide();
    this.getAllUsersLists();
  }

  getAllUsersLists(){
    this.service.getAllUsersLists().subscribe(
      (response: any) => {
        this.totalUsersArray = response.message;
        this.UsersArray = response.message.slice(0, 10);
        if(this.UsersArray.length == 0){
          this.norecordfound = true;
        }else{
          this.norecordfound = false;

        }
        $('.full_page_loader_app').hide();
		$('.dashboard-table-outer').css('opacity',1).show();
      },
      err => {
        console.log("err", err)
      }
    );
  }

  searchUsers(e:any){

    let data = {
      searchValue : this.searchText
    }
    // console.log(data);
    this.service.searchUsers(data).subscribe(
      (response: any) => {
        this.totalUsersArray = response.message;
        this.UsersArray = response.message.slice(0, 10);

        // console.log("this.UsersArray", this.UsersArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }

  OnPageEvent(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.totalUsersArray.length) {
      endIndex = this.totalUsersArray.length;
    }
    this.Index = startIndex + 1;
    this.UsersArray = this.totalUsersArray.slice(startIndex, endIndex);
  }

}
