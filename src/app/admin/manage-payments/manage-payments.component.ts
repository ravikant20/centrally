import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-manage-payments',
  templateUrl: './manage-payments.component.html',
  styleUrls: ['./manage-payments.component.css']
})
export class ManagePaymentsComponent implements OnInit {
  totalDonationdata: any = [];
  Donationdata: any = [];
  bookingsArray: any = [];
  norecordfound: any = false;
  length: any = 10;
  pageSize: any = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  Index: any = 1;
  recent_bookings_data: any = [];
  sessionDatearray: any = [];
  subcategorylistData: any;
  subArray: any;
  
  
  constructor(private service: ServiceFileService) { }

  ngOnInit(): void {
    $('.full_page_loader_app').show();
    $('.dashboard-table-outer').css('opacity', 0).hide();
    this.getDonationdata();
    // this.subcatListing();

  }

 
  getDonationdata() {
    this.service.getDonationdata().subscribe(
      (response: any) => {
        this.totalDonationdata = response.message;
        this.Donationdata = response.message.slice(0, 10);
        if (this.Donationdata.length == 0) {
          this.norecordfound = true;
        }else{
          this.norecordfound = false;

        }
        $('.full_page_loader_app').hide();
        $('.dashboard-table-outer').css('opacity', 1).show();
      },
      err => {
        console.log("err", err)
      }
    )
  }



  OnPageEvent(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.totalDonationdata.length) {
      endIndex = this.totalDonationdata.length;
    }
    this.Index = startIndex + 1;
    this.Donationdata = this.totalDonationdata.slice(startIndex, endIndex);
  }

  formatDate(date: any) {
    var d = new Date(date),
      month = '' + (d.getUTCMonth() + 1),
      day = '' + d.getUTCDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  formatTime(time: any) {
    return moment(time, 'hh:mm').format('LT');
  }

  getTime(data:any){
    var d = new Date(data);
    var h = d.getUTCHours();
    var m = d.getUTCMinutes();
    return h+':'+m;
  }

}
