import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import * as $ from 'jquery';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { PageEvent } from '@angular/material/paginator';


@Component({
	selector: 'app-adminclasses',
	templateUrl: './adminclasses.component.html',
	styleUrls: ['./adminclasses.component.css'],
	encapsulation: ViewEncapsulation.None,
})
export class AdminclassesComponent implements OnInit {
	classArray: any = [];
	totalclassArray: any = [];
	categoriesArray: any = [];
	TeachersArray: any = [];
	booleanUpdate: any = false
	norecordfound: any = false;
	submitdisable: any = false;
	subCategories_text: any = "";
	no_of_repetitions: any = "";
	stype:any = "";
	sessionDatearray: any = [];
	activeId: any = "";
	fd = new FormData();
	file: any;
	searchText: string = "";
	searchArray: any = [];
	data: any = [];
	viewclassarray: any = [];
	date1: any = [];
	formattedDate: any = [];
	selectElementText: any;
	subcategorylistData: any;
	subArray: any;
	subCategories_id: any;
	ageGroup: string = " Years";
	minimumDate = new Date();
	price: any = "";
	priceError: any = "";
	title: any = "";
	categories: any = "";
	start_date: any = "";
	start_time: any = "";
	end_time: any = "";
	teacher: any = "";
	individual_price: any = "";
	siblings_price: any = "";
	users_limit:any = 0;
	individual_priceError: any = "";
	siblings_priceError: any = "";
	myGroup = new FormGroup({});
	length: any = 10;
	pageSize: any = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	Index: any = 1;
	classexit: boolean = false;
	subscriptionerror: any = "";
	constructor(private _fb: FormBuilder, private service: ServiceFileService) { }


	ngOnInit(): void {

		$('.full_page_loader_app').show();
		$('.dashboard-table-outer').css('opacity', 0).hide();


		this.myGroup = this._fb.group({
			'title': ['', [Validators.required]],
			'categories': ['', [Validators.required]],
			'subCategories_id': ['', [Validators.required]],
			'start_date': ['', [Validators.required]],
			'start_time': ['', [Validators.required]],
			'end_time': ['', [Validators.required]],
			'teacher': ['', [Validators.required]],
			'individual_price': ['', [Validators.required]],
			'siblings_price': ['', [Validators.required]],
			'users_limit':['',[Validators.required]]


		});

		this.getClasses()
		this.getTeachers();
		this.getCategories();
		this.subcatListing();
	}

	validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
			const control = myGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}

	pricevalid(event: any) {
		const $this = this;
		$this.individual_priceError = false;
		$this.siblings_priceError = false;

		var validatePrice = function (price: any) {
			return /^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(price);
		}

		if ($(event.target).hasClass('individual_price')) {
			let price = $(event.target).val();
			if (validatePrice(price) == false) {
				$this.individual_priceError = true;

			}
		}
		if ($(event.target).hasClass('siblings_price')) {
			let price = $(event.target).val();
			if (validatePrice(price) == false) {
				$this.siblings_priceError = true;

			}

		}




	}


	getClasses() {
		this.service.getClasses().subscribe(
			(response: any) => {
				this.totalclassArray = response.message;
				this.classArray = response.message.slice(0, 10);
				if (this.classArray.length == 0) {
					this.norecordfound = true;
				} else {
					this.norecordfound = false;

				}
				$('.full_page_loader_app').hide();
				$('.dashboard-table-outer').css('opacity', 1).show();
			},
			err => {
				console.log("err", err)
			}
		);
	}

	onFileSelected(event: any) {
		let fileList: FileList = event.target.files;
		let file: File = fileList[0];
		this.file = fileList[0];
		//this.fd.append('file', file, file.name);
		// this.selectedFile = event.target.files[0];
	}


	getTeachers() {
		this.service.getTeachers().subscribe(
			(response: any) => {
				this.TeachersArray = response.message;

			},
			err => {
				console.log("err", err)
			}
		);
	}

	getCategories() {
		this.service.getCategories().subscribe(
			(response: any) => {
				this.categoriesArray = response.message;
				// console.log("this.categoriesArray ", this.categoriesArray)
			},
			err => {
				console.log("err", err)
			}
		);
	}


	changeStatus(post: any, event: any) {

		var status = 0;
		if (event.srcElement.checked) {
			if (confirm('Are you sure you want to Active this teacher')) {

				status = 1;
			} else {
				event.srcElement.checked = !event.srcElement.checked
			}
		}
		else {
			if (confirm('Are you sure you want to inctive this teacher')) {
				status = 0;
			} else {
				event.srcElement.checked = !event.srcElement.checked
			}
		}
		var daa = {
			id: post.id,
			status: status
		}

		this.service.updateTeachersStatus(daa).subscribe(
			(response: any) => {
				this.getClasses()
				alert("Status changed")
			},
			err => {
				console.log("err", err)
			}
		);
	}

	SaveClass(e: any) {

		this.subscriptionerror = "";
		this.validateAllFields(this.myGroup);


if(this.users_limit == 0 && this.stype != 1){
	this.users_limit = "";
	return false;
}

		if (this.myGroup.valid && !this.classexit) {
			this.submitdisable = true;
			if (this.data.type == '1') {
				this.data.capacity = 1;
			}


			if (!this.booleanUpdate) {
				const formdate = new FormData();
				if (this.file) {
					formdate.append('file', this.file, this.file.name);
				}

				formdate.append('category', this.categories)

				formdate.append('subcategory', this.subCategories_id)

				formdate.append('teacher', this.teacher)

				formdate.append('start_date', this.start_date)

				formdate.append('end_time', this.end_time)
				formdate.append('start_time', this.start_time)
				formdate.append('individual_price', this.individual_price)
				formdate.append('siblings_price', this.siblings_price)
				formdate.append('users_limit',this.users_limit)
				formdate.append('title', this.title)
				this.service.addClass(formdate).subscribe(
					(response: any) => {
						if (response.error == 1 && response.message == "exist") {
							this.classexit = true;
						}
						else if (response.error == 1) {
							this.subscriptionerror = response.message;
						}
						else {
							$('button.close').trigger('click');
							this.data = [];
							this.getClasses();
						}
						this.submitdisable = false;
						this.booleanUpdate = false;


					},
					err => {
						this.submitdisable = false;
						console.log("err", err)
					}
				);
			}

		}
	}


	UpdateClass(e: any) {

		this.subscriptionerror = "";

		this.validateAllFields(this.myGroup);
		if(this.users_limit == 0 && this.stype != 1){
			this.users_limit = "";
			return false;
		}
		
		if (this.myGroup.valid && !this.classexit) {
			this.submitdisable = true;


			if (this.data.type == '1') {
				this.data.capacity = 1;
			}


			if (this.booleanUpdate) {
				const formdate = new FormData();
				formdate.append('category', this.categories)
				formdate.append('subcategory', this.subCategories_id)
				formdate.append('teacher', this.teacher)
				formdate.append('start_date', this.start_date)
				formdate.append('end_time', this.end_time)
				formdate.append('start_time', this.start_time)
				formdate.append('title', this.title)
				formdate.append('individual_price', this.individual_price)
				formdate.append('siblings_price', this.siblings_price)
				formdate.append('users_limit',this.users_limit)
				formdate.append('id', this.activeId)

				if (this.file) {
					formdate.append('file', this.file, this.file.name);
				}
				this.service.updateClass(formdate).subscribe(
					(response: any) => {
						if (response.error == 1 && response.message == "exist") {
							this.classexit = true;
						
						}
						else if (response.error == 1) {
							this.subscriptionerror = response.message;
						} else {
							$('button.close').trigger('click');
							this.data = [];
							this.getClasses();
						}
						this.submitdisable = false;
						// this.booleanUpdate = false;

					},
					err => {
						console.log("err", err)
						this.submitdisable = false;
					}


				);
			}
		}

	}


	updateData(post: any, id: any) {
		this.classexit = false;
		this.subscriptionerror = "";

		this.booleanUpdate = true;
		this.submitdisable = false;
		this.no_of_repetitions = "";
		// console.log(post);
		var todayDate = new Date().toISOString().slice(0, 10);
		this.data.categories = post.categories_id;
		this.data.subCategories_id = post.subCategories_id;
		this.data.teacher = post.teachers_id;
		this.data.start_date = post.start_date;
		this.data.start_time = post.start_time;
		this.data.end_time = post.end_time;
		this.data.stype = post.subcategories.stype;
		// this.data.start_time = new Date(todayDate + " " + post.start_time);
		// this.data.end_time = new Date(todayDate + " " + post.end_time);
		this.data.title = post.title;
		this.data.individual_price = post.individual_price;
		this.data.siblings_price = post.siblings_price;
		this.data.users_limit = post.users_limit;
		this.data.payment_one_student = post.payment_one_student;
		this.data.payment_one_plus_student = post.payment_one_plus_student;
		this.data.payment_two_plus_student = post.payment_two_plus_student;
		this.data.end_date = new Date(post.end_date);
		this.activeId = id;

		this.title = this.data.title;
		this.categories = this.data.categories;
		this.subCategories_id = this.data.subCategories_id;
		this.start_date = this.data.start_date;
		this.start_time = this.data.start_time;
		this.end_time = this.data.end_time;
		this.teacher = this.data.teacher;
		this.individual_price = this.data.individual_price;
		this.siblings_price = this.data.siblings_price;
		this.users_limit = this.data.users_limit;
		this.stype = this.data.stype;

		this.getSelectedOptionbyid(post.categories_id);
	}

	deleteClasses(id: any) {

	}

	getSelectedOptionbyid($id: any) {

		this.selectElementText = $id;

		this.subArray = this.subcategorylistData.filter((x: any) => x.cat_type == this.selectElementText);

	}

	convertData(date: any) {
		var months = new Array(12);
		months[0] = "January";
		months[1] = "February";
		months[2] = "March";
		months[3] = "April";
		months[4] = "May";
		months[5] = "June";
		months[6] = "July";
		months[7] = "August";
		months[8] = "September";
		months[9] = "October";
		months[10] = "November";
		months[11] = "December";

		var current_date = new Date(date);
		current_date.setUTCDate(current_date.getUTCDate() + 15);
		var month_value = current_date.getUTCMonth();
		var day_value = current_date.getUTCDate();
		var year_value = current_date.getUTCFullYear();

		return day_value + " " + months[month_value] + ", " + year_value
	}

	viewclass(post: any) {
		const $this = this;
		this.viewclassarray = post
		if (post) {
			// console.log(post.subCategories_id);
			this.getSubCategoriesbyidn(post);


		}
		// }



	}




	searchClasses(e: any) {
		let data = {
			searchValue: this.searchText
		}

		this.service.searchClasses(data).subscribe(
			(response: any) => {
				this.totalclassArray = response.message;
				this.classArray = response.message.slice(0, 10);

			},
			err => {
				console.log("err", err)
			}
		);
	}

	getSelectedOptionText(event: any) {
		// debugger
		let selectedOptions = event.target['options'];
		let selectedIndex = selectedOptions.selectedIndex;
		this.selectElementText = selectedOptions[selectedIndex].value;

		this.subArray = this.subcategorylistData.filter((x: any) => x.cat_type == this.selectElementText);
		this.subCategories_id = '';
	}


	subcatListing() {
		this.service.subcatListing().subscribe(
			(response: any) => {

				this.subcategorylistData = this.subArray = response.data;

			}, (err: any) => {
				console.log(err)
			}
		)
	}


	deleteClass(id: any) {

		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			confirmButtonColor: '#738c72',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.isConfirmed) {
				const data = {
					id: id
				}
				this.service.deleteClass(data).subscribe(
					(response: any) => {

						if (response.message == "true") {
							this.getClasses();
						} else if (response.error == 1) {

							Swal.fire({
								title: 'Error',
								text: response.message,
								icon: 'warning',
								confirmButtonColor: '#738c72'
							});

						}
					}, (err: any) => {
						console.log(err)
					}
				)
			}
		})




	}


	addClass(event: any) {
		this.classexit = false;
		this.subscriptionerror = "";
		this.activeId = "";
		this.submitdisable = false;
		this.booleanUpdate = false;
		this.no_of_repetitions = "";
		this.stype = "";
		$('#create-class').find('form').trigger('reset');

	}

	selectsubCategory(event: any) {
		if (this.categories == 1) {
			// console.log(event.target.innerText);
			this.subCategories_id = event.target.value;
			
		}
		this.getSubCategoriesbyid(event.target.value);
	}

	getSubCategoriesbyid(id: any) {
		const data = {
			id: id
		}
		this.service.getSubCategoriesbyid(data).subscribe(
			(response: any) => {
				this.no_of_repetitions = response.message.no_of_repetitions;
				this.stype = response.message.stype;
				if(this.stype == 1){
						this.users_limit = 0;

				}else{
					 this.users_limit = "";
				}
				// this.subcategorylistData = response.data;

			}, (err: any) => {
				console.log(err)
			}
		)
	}

	
	getSubCategoriesbyidn(post: any) {
		this.sessionDatearray = [];
		if (post.subCategories_id) {
			const data = {
				id: post.subCategories_id
			}
			this.service.getSubCategoriesbyid(data).subscribe(
				(response: any) => {
					let no_of_repetitions = response.message.no_of_repetitions;
					this.stype = response.message.stype;
					if(this.stype == 1){
						this.users_limit = 0;

				}else{
					this.users_limit = "";
				}
					if (response.message.stype != 1) {
						var startDate = new Date(post.start_date);
						for (var i = 0; i < no_of_repetitions; i++) {
							if (i == 0) {
								var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

							} else {
								var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

							}
							let formatdate = this.formatDate(date);
							this.sessionDatearray.push(formatdate);
						
						}
						
					}

				}, (err: any) => {
					console.log(err)
				}
			);

			
		}

	}

	 formatDate(date: any) {
		var d = new Date(date),
			month = '' + (d.getUTCMonth() + 1),
			day = '' + d.getUTCDate(),
			year = d.getUTCFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [year, month, day].join('-');
	}






	formatTime(time: any) {
		return moment(time, 'hh:mm').format('LT');
	}

	OnPageEvent(event: PageEvent) {
		const startIndex = event.pageIndex * event.pageSize;
		let endIndex = startIndex + event.pageSize;
		if (endIndex > this.totalclassArray.length) {
			endIndex = this.totalclassArray.length;
		}
		this.Index = startIndex + 1;
		this.classArray = this.totalclassArray.slice(startIndex, endIndex);
	}

	selecttutor(event: any) {
		this.classexit = false;
		this.submitdisable = true;

		// console.log(event.target.value);
		const data = {
			teacher: this.teacher,
			start_date: this.start_date,
			start_time: this.start_time,
			end_time: this.end_time,
			id: this.activeId,
			subCategories_id: this.subCategories_id
		}
		this.service.checkClass(data).subscribe(
			(response: any) => {

				if (response.error == 1 && response.message == "exist") {
					this.classexit = true;
				}
				this.submitdisable = false;
			}, (err: any) => {
				console.log(err)
			}

		)
	}

	selectdate(event: any) {
		this.classexit = false;
		this.submitdisable = true;

		// console.log(event.target.value);
		const data = {
			teacher: this.teacher,
			start_date: this.start_date,
			start_time: this.start_time,
			end_time: this.end_time,
			id: this.activeId,
			subCategories_id: this.subCategories_id
		}
		this.service.checkClass(data).subscribe(
			(response: any) => {

				if (response.error == 1 && response.message == "exist") {
					this.classexit = true;
				}
				this.submitdisable = false;
			}, (err: any) => {
				console.log(err)
			}

		)
	}

	selecttime(event: any) {
		this.classexit = false;
		this.submitdisable = true;

		// console.log(event.target.value);
		const data = {
			teacher: this.teacher,
			start_date: this.start_date,
			start_time: this.start_time,
			end_time: this.end_time,
			id: this.activeId,
			subCategories_id: this.subCategories_id
		}
		this.service.checkClass(data).subscribe(
			(response: any) => {

				if (response.error == 1 && response.message == "exist") {
					this.classexit = true;
				}
				this.submitdisable = false;
			}, (err: any) => {
				console.log(err)
			}

		)
	}

	checkuserlimit(event:any){
		let $value = event.target.value;
	if($value){
		if($value == 0){
			this.users_limit = "";
	}  
}
}


}