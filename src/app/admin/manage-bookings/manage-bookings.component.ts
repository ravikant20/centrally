import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { PageEvent } from '@angular/material/paginator';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import * as $ from 'jquery';


@Component({
	selector: 'app-manage-bookings',
	templateUrl: './manage-bookings.component.html',
	styleUrls: ['./manage-bookings.component.css']
})
export class ManageBookingsComponent implements OnInit {
	totalrecentBookings: any = [];
	recentBookings: any = [];
	bookingsArray: any = [];
	norecordfound: any = false;
	length: any = 10;
	pageSize: any = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	Index: any = 1;
	recent_bookings_data: any = [];
	sessionDatearray: any = [];
	subcategorylistData: any;
	subArray: any;
	searchText: string = "";
	searchArray: any = [];
	classArray: any = [];
	totalclassArray: any = [];
	categoriesArray: any = [];
	TeachersArray: any = [];
	booleanUpdate: any = false
	classesDatesArray:any = [];
	newarrayt:any = [];
	submitdisable: any = false;
	subCategories_text: any = "";
	no_of_repetitions: any = "";
	stype: any = "";
	interval:any = "";
	activeId: any = "";
	fd = new FormData();
	file: any;
	data: any = [];
	viewclassarray: any = [];
	date1: any = [];
	formattedDate: any = [];
	selectElementText: any;
	subCategories_id: any;
	ageGroup: string = " Years";
	minimumDate = new Date();
	price: any = "";
	priceError: any = "";
	title: any = "";
	categories: any = "";
	start_date: any = new Date();
	start_time: any = "";
	end_time: any = "";
	teacher: any = "";
	individual_price: any = "";
	siblings_price: any = "";
	users_limit: any = 0;
	individual_priceError: any = "";
	siblings_priceError: any = "";
	classexit: boolean = false;
	subscriptionerror: any = "";
	category_id: any = "";
	subCategoriesid: any = "";
	time1: any = [];
	time2: any = [];
	todayDay: any = ""
	formatedTimeSlot1: any = [];
	formatedTimeSlot2: any = [];
	categoriEsteacherWise: any = [];
	className: any = [];
	classNameArray: any = [];
	tutorName: any = [];
	tutorNameArray: any = [];
	classname: any = [];
	classidArray: any = [];
	classidval: any = "";
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];

	dayNames: any = [
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		"Sunday"
	];
	validDate: Date[] = [new Date()];
	showtotaldefaultslot: any = 12;
	slotval: any = "";
	classDate: any = [];
	number_of_users: any = "";
	user_id: any = "";
	myGroup = new FormGroup({});
	constructor(private _fb: FormBuilder, private service: ServiceFileService) { }

	ngOnInit(): void {
		$('.full_page_loader_app').show();
		$('.dashboard-table-outer').css('opacity', 0).hide();
		this.getRecentBookings();
		this.subcatListing();
		this.getTeachers();
		this.scriptforall();
		this.myGroup = this._fb.group({
			'title': ['', [Validators.required]],
			'start_date': ['', [Validators.required]],
			'start_time': ['', [Validators.required]],
			'end_time': ['', [Validators.required]],
			'teacher': ['', [Validators.required]],
			'users_limit': ['', [Validators.required]]


		});

	}


	getRecentBookings() {
		this.service.getRecentBookings().subscribe(
			(response: any) => {
				this.totalrecentBookings = response.message;
				this.recentBookings = response.message.slice(0, 10);
				if (this.recentBookings.length == 0) {
					this.norecordfound = true;
				} else {
					this.norecordfound = false;

				}
				$('.full_page_loader_app').hide();
				$('.dashboard-table-outer').css('opacity', 1).show();
			},
			err => {
				console.log("err", err)
			}
		)
	}



	viewBookings(post: any) {
		this.bookingsArray = post;

		//console.log("View Booking details",this.bookingsArray.title);

		this.recent_bookings_data = {
			title: this.bookingsArray.classes.title,
			categories: this.bookingsArray.categoriesData,
			categories_id: this.bookingsArray.classes.categories_id,
			subCategories_id: this.bookingsArray.classes.subCategories_id,
			numberOfUsers: this.bookingsArray.number_of_users,
			start_date: this.bookingsArray.classes.start_date,
			start_time: this.bookingsArray.classes.start_time,
			end_time: this.bookingsArray.classes.end_time,
			teachers_name: this.bookingsArray.teachers_name,
			amount: this.bookingsArray.amount,
			stype: this.bookingsArray.categoriesData.stype,
			siblingsData: this.bookingsArray.siblingsData,
			issiblingsData: this.bookingsArray.issiblingsData,
			individual: this.bookingsArray.individual
		}

		if (post) {

			if (post.categoriesData.stype == 1) {


				this.sessionDatearray = [];
				var startDate = new Date(post.classes.start_date);
				for (var i = 0; i < (post.number_of_week + 1); i++) {
					if (i == 0) {
						var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

					} else {
						var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

					}
					let formatdate = this.formatDate(date);
					if (formatdate >= this.formatDate(new Date())) {
						this.sessionDatearray.push(formatdate);
					}
				}

			} else {
				this.getSubCategoriesbyidn(post.classes);
			}
		}
		// console.log(this.recent_bookings_data);
	}


	getSubCategoriesbyidn(post: any) {
		this.sessionDatearray = [];
		if (post.subCategories_id) {
			const data = {
				id: post.subCategories_id
			}
			this.service.getSubCategoriesbyid(data).subscribe(
				(response: any) => {
					let no_of_repetitions = response.message.no_of_repetitions;

					var startDate = new Date(post.start_date);
					for (var i = 0; i < no_of_repetitions; i++) {
						if (i == 0) {
							var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 0 * 7));

						} else {
							var date = new Date(startDate.setUTCDate(startDate.getUTCDate() + 1 * 7));

						}
						let formatdate = this.formatDate(date);
						this.sessionDatearray.push(formatdate);
					}

				}, (err: any) => {
					console.log(err)
				}
			);


		}

	}


	formatDate(date: any) {
		var d = new Date(date),
			month = '' + (d.getUTCMonth() + 1),
			day = '' + d.getUTCDate(),
			year = d.getUTCFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [year, month, day].join('-');
	}

	formatTime(time: any) {
		return moment(time, 'hh:mm').format('LT');
	}

	subcatListing() {
		this.service.subcatListing().subscribe(
			(response: any) => {

				this.subcategorylistData = this.subArray = response.data;

			}, (err: any) => {
				console.log(err)
			}
		)
	}

	OnPageEvent(event: PageEvent) {
		const startIndex = event.pageIndex * event.pageSize;
		let endIndex = startIndex + event.pageSize;
		if (endIndex > this.totalrecentBookings.length) {
			endIndex = this.totalrecentBookings.length;
		}
		this.Index = startIndex + 1;
		this.recentBookings = this.totalrecentBookings.slice(startIndex, endIndex);
	}

	searchBookings(e: any) {

		let data = {
			searchValue: this.searchText
		}
		// console.log(data);
		this.service.searchBookings(data).subscribe(
			(response: any) => {
				this.totalrecentBookings = response.message;
				this.recentBookings = response.message.slice(0, 10);

				// console.log("this.UsersArray", this.UsersArray)
			},
			err => {
				console.log("err", err)
			}
		);
	}
	validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
			const control = myGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}


	UpdateBooking(e: any) {

		this.checkuserlimitforclass(this.classidval, this.number_of_users);



	}


	selecttime(event: any) {
		this.classexit = false;
		this.submitdisable = true;

		// console.log(event.target.value);
		const data = {
			teacher: this.teacher,
			start_date: this.start_date,
			start_time: this.start_time,
			end_time: this.end_time,
			id: this.activeId,
			subCategories_id: this.subCategories_id
		}
		this.service.checkClass(data).subscribe(
			(response: any) => {

				if (response.error == 1 && response.message == "exist") {
					this.classexit = true;
				}
				this.submitdisable = false;
			}, (err: any) => {
				console.log(err)
			}

		)
	}

	selectdate(event: any) {
		this.classexit = false;
		this.submitdisable = true;

		// console.log(event.target.value);
		const data = {
			teacher: this.teacher,
			start_date: this.start_date,
			start_time: this.start_time,
			end_time: this.end_time,
			id: this.activeId,
			subCategories_id: this.subCategories_id
		}
		this.service.checkClass(data).subscribe(
			(response: any) => {

				if (response.error == 1 && response.message == "exist") {
					this.classexit = true;
				}
				this.submitdisable = false;
			}, (err: any) => {
				console.log(err)
			}

		)
	}

	selecttutor(event: any) {
		this.classexit = false;
		this.submitdisable = true;

		// console.log(event.target.value);
		const data = {
			teacher: this.teacher,
			start_date: this.start_date,
			start_time: this.start_time,
			end_time: this.end_time,
			id: this.activeId,
			subCategories_id: this.subCategories_id
		}
		this.service.checkClass(data).subscribe(
			(response: any) => {

				if (response.error == 1 && response.message == "exist") {
					this.classexit = true;
				}
				this.submitdisable = false;
			}, (err: any) => {
				console.log(err)
			}

		)
	}


	updateData(post: any, id: any) {
		clearInterval(this.interval);
		this.getSlotBycategory(post.classes.categories_id,post.classes.subCategories_id );
		this.formatedTimeSlot1 = [];
		$('.error-msg').removeAttr('id').html('');
		this.classexit = false;
		this.subscriptionerror = "";

		this.booleanUpdate = true;
		this.submitdisable = false;
		this.no_of_repetitions = "";
		// console.log(post);
		var todayDate = new Date().toISOString().slice(0, 10);
		this.data.categories = post.classes.categories_id;
		this.data.subCategories_id = post.classes.subCategories_id;
		this.data.teacher = post.classes.teachers_id;
		this.data.start_date = post.classes.start_date;
		this.data.start_time = post.classes.start_time;
		this.data.end_time = post.classes.end_time;
		this.data.users_limit = post.classes.users_limit;
		this.data.user_id = post.user_id;
		this.recent_bookings_data = {
			siblingsData: post.siblingsData,
			issiblingsData: post.issiblingsData,
			individual: post.individual
			// numberOfUsers: post.number_of_users,

		}
		this.number_of_users = post.number_of_users;
		// this.data.stype = post.subcategories.stype;
		// this.data.start_time = new Date(todayDate + " " + post.start_time);
		// this.data.end_time = new Date(todayDate + " " + post.end_time);
		this.data.title = post.classes.title;
		this.data.end_date = new Date(post.classes.end_date);
		this.activeId = post.id;

		this.title = this.data.title;
		this.categories = this.data.categories;
		this.subCategories_id = this.data.subCategories_id;
		this.start_date = this.convertDateToUtcDate(new Date(this.data.start_date));
		// this.start_date = this.start_date.toUTCString();
		this.start_time = this.data.start_time;
		this.end_time = this.data.end_time;
		this.teacher = this.data.teacher;
		this.users_limit = this.data.users_limit;
		this.stype = this.data.stype;
		this.user_id = this.data.user_id;
		// this.getSelectedOptionbyid(post.categories_id);
	}


	convertDateToUtcDate(datestring:any){
		var utc = new Date(datestring.getTime() + datestring.getTimezoneOffset() * 60000);
		return utc;
	}

	getSlotBycategory($pid: any, $sid: any) {

		const $this = this;
		$('.singledate').removeAttr('id');
		// this.date1 = new Date();
		this.categoriEsteacherWise = []
		this.formatedTimeSlot1 = []

		var data = {
			date: this.todayDay,
			categories_id: $pid,
			subCategories_id: $sid
		}

		this.service.getSlotBycategory(data).subscribe(
			(response: any) => {

				this.classesDatesArray = response.message;
				this.newarrayt = [];
				var fulldatenewarray: any = [];

				if (response.message) {
					if ($pid == 1) {
						this.formatedTimeSlot1 = [];
						this.classNameArray = [];
						this.tutorNameArray = [];
						this.classidArray = [];
						this.classDate = [];
					}
					for (var i = 0; i < this.classesDatesArray.length; i++) {
						if(this.teacher == this.classesDatesArray[i].teachers_id){
						if ($pid == 1) {
							this.time1 = this.classesDatesArray[i].start_time;
							this.time2 = this.classesDatesArray[i].end_time;
							this.className = this.classesDatesArray[i].title;
							this.tutorName = this.classesDatesArray[i].teachers_name;
							this.classNameArray.push(this.className);
							let fulldate = new Date(this.classesDatesArray[i].start_date);
							this.classDate.push({ date: this.classesDatesArray[i].start_date, day: $this.dayNames[fulldate.getUTCDay()] });
							this.tutorNameArray.push(this.tutorName);
							// this.formatedTimeSlot1.push(moment(this.time1, 'hh:mm').format('LT') + " to " + moment(this.time2, 'hh:mm').format('LT'));
							this.classidArray.push(this.classesDatesArray[i].id);
						}

						var startdate = this.classesDatesArray[i].start_date;
						let fulldate = new Date(startdate);
						fulldatenewarray.push(fulldate);
						let y = fulldate.getUTCFullYear();
						let d = fulldate.getUTCDate();
						let m = $this.monthNames[fulldate.getUTCMonth()];
						let cnewarray = { 'y': y, 'm': m, 'd': d };
						this.newarrayt.push(cnewarray);

						this.validDate.push(fulldate);
					}
					}
					this.classidval = this.classidArray[0];


				}
				$('.singledate').attr('id', '');

				if (this.newarrayt.length > 0) {
					$('.singledate').attr('id', 'data-found' + $pid + $sid);
				}




				// this.invalidDates = fulldatenewarray;


				this.interval = setInterval(function () {

					if ($this.newarrayt.length > 0) {

						for (var $i = 0; $i < $this.newarrayt.length; $i++) {

							var d = ($this.newarrayt[$i].d ? $this.newarrayt[$i].d : '');

							var y = ($this.newarrayt[$i].y ? $this.newarrayt[$i].y : '');
							var m = ($this.newarrayt[$i].m ? $this.newarrayt[$i].m : '');


							$('#data-found' + $pid + $sid + ' .p-datepicker-calendar tbody tr td').each(function () {
								if (!$(this).hasClass('p-datepicker-other-month')) {
									let monthhtml = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-month').text();
									let yearhtml = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-year').text();

									if (parseInt($(this).find('span').text()) == parseInt(d) && monthhtml.toLowerCase() == m.toLowerCase() && yearhtml == y) {
										if (!$(this).hasClass('cactive')) {
											$(this).addClass('cactive');


										}
									}

								}
							});


						}
					}
				});



			},
			err => {
				console.log("err", err)
			}
		)

	}

	getTeachers() {
		this.service.getTeachers().subscribe(
			(response: any) => {
				this.TeachersArray = response.message;

			},
			err => {
				console.log("err", err)
			}
		);
	}


	selectDate(event: any) {


		var today = new Date(event);
		var daysss = String(today.getMonth() + 1)
		if (daysss.toString().length == 1) {
			daysss = "0" + daysss;
		}
		this.todayDay = today.getFullYear() + '-' + daysss + '-' + today.getDate();
		this.getSlotsandTeacher();

		this.validDate = [today];
		$('.error-msg').removeAttr('id').html('');



	}

	getSlotsandTeacher() {
		const $this = this;

		this.category_id = this.categories;
		this.subCategoriesid = this.subCategories_id;

		var data = {
			date: this.todayDay,
			categories_id: this.category_id,
			subCategories_id: this.subCategoriesid

		}


		this.service.getRecordsWithCategories(data).subscribe(
			(response: any) => {

				if (response.status == 1) {
					if (response.message != "") {
						this.categoriEsteacherWise = response.message
						this.formatedTimeSlot1 = [];
						this.classNameArray = [];
						this.tutorNameArray = [];
						this.classidArray = [];

						for (var i = 0; i < this.categoriEsteacherWise.length; i++) {

							this.time1 = this.categoriEsteacherWise[i].start_time;
							this.time2 = this.categoriEsteacherWise[i].end_time;
							this.className = this.categoriEsteacherWise[i].title;
							this.tutorName = this.categoriEsteacherWise[i].teachers_name;
							this.classNameArray.push(this.className);
							this.tutorNameArray.push(this.tutorName);
							this.formatedTimeSlot1.push(moment(this.time1, 'hh:mm').format('LT') + " to " + moment(this.time2, 'hh:mm').format('LT'));
							this.classidArray.push(this.categoriEsteacherWise[i].id);

						}

						this.classidval = this.classidArray[0];
						this.title = this.classNameArray[0];

						setInterval(function () {
							let fulldate = new Date($this.todayDay);
							let y: any = fulldate.getUTCFullYear();
							let d: any = fulldate.getUTCDate();
							let m: any = $this.monthNames[fulldate.getUTCMonth()];

							$('#data-found' + $this.category_id + $this.subCategoriesid + ' .p-datepicker-calendar tbody tr td').each(function () {
								if (!$(this).hasClass('p-datepicker-other-month')) {
									let monthhtml: any = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-month').text();
									let yearhtml: any = $(this).closest('.p-datepicker-calendar-container').prev('.p-datepicker-header').find('.p-datepicker-year').text();

									if (parseInt($(this).find('span').text()) == parseInt(d) && monthhtml.toLowerCase() == m.toLowerCase() && yearhtml == y) {
										if (!$(this).find('span').hasClass('p-highlight')) {
											$(this).find('span').addClass('p-highlight');


										}
									}
								}
							});

						});

					} else {
						this.categoriEsteacherWise = "";
						this.classNameArray = [];
						this.tutorNameArray = [];
						this.formatedTimeSlot1 = [];
					}


				} else {
					console.log('error');
				}



			},
			err => {
				console.log("err", err)
			}
		);
	}

	showmoreSlot(event: any) {
		const $this = this;
		$(".time-slot.hide").each(function (n: any) {
			if (parseInt(n + 1) <= $this.showtotaldefaultslot) {
				$(this).removeClass('hide');
			}
		});
		if ($(".time-slot.hide").length > 0) {
			$('.show-more').show();
		} else {
			$('.show-more').hide();

		}
	}

	getclassbyslot(classid: any, classname: any) {
		const $this = this;
		this.classidval = classid;
		this.title = classname;


	}

	checkuserlimitforclass(class_id: any, number_of_users: any) {
		const data = {
			'class_id': class_id,
			'number_of_users': number_of_users
		}

		this.service.checkuserlimitforclass(data).subscribe(
			(response: any) => {
				if (response.error == 0 && response.message == true && response.aviable == 0) {

					$('.error-msg').attr('id', 'show').html("This class has reached maximum users limit. Please select another class");

				} else if (response.error == 0 && response.message == true && response.aviable > 0) {
					$('.error-msg').attr('id', 'show').html("There are only " + response.aviable + " slots left. Please  update the users added.");
				}
				else {

					const formdata = {
						id: this.activeId,
						user_id: this.data.user_id,
						class_id: class_id
					};

					this.service.updateBookingClass(formdata).subscribe(
						(response: any) => {
							if (response.error == 1 && response.message == "exist") {
								this.classexit = true;

							}
							else if (response.error == 1) {
								this.subscriptionerror = response.message;
							} else {
								$('button.close').trigger('click');
								this.data = [];
								this.getRecentBookings();
							}
							this.submitdisable = false;
							// this.booleanUpdate = false;

						},
						err => {
							console.log("err", err)
							this.submitdisable = false;
						}


					);
				}
			});

	}

	scriptforall(){
		const $this = this;
		$(document).on('click', '.time-slot', function () {
			$('.time-slot').find('.radio_slot').prop('checked', false);
			$(this).find('.radio_slot').prop('checked', true);
			$this.classidval = $(this).find('.class_id_slot').val();
			$this.slotval = $(this).find('.radio_slot').val();

		});

	}
	// getclassdatabyid(classid:any){

	// }

}
