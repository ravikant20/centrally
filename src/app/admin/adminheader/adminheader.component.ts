import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-adminheader',
  templateUrl: './adminheader.component.html',
  styleUrls: ['./adminheader.component.css']
})
export class AdminheaderComponent implements OnInit {

  loggedInUser: any = ""
  image:any = [];
  name:any = [];
  mainurl:any = environment.PROJECT_URL;
  constructor(private router: Router) { 
    // if (localStorage.getItem('role') == '1') {
      
      // this.router.navigate(['/admin/login']);
    // }else {
    //   this.router.navigate(['/admin/dashboard']);
    // }
  }

  ngOnInit(): void {
    

      if (localStorage.getItem('role') == '1') {
        this.loggedInUser = "admin";
       
  
        // this.router.navigate(['/admin/login']);
      } else if (localStorage.getItem('role') == '2') {
        this.loggedInUser = "user"
        this.router.navigate(['/']);
      }
      else {
        this.loggedInUser = "guest"
        this.router.navigate(['/']);

      }
      if(localStorage.getItem('image')){
        this.image = localStorage.getItem('image');
      }else{
        this.image = this.mainurl+"/centrallyRooted/public/uploads/1625737388.png";
      }
      if(localStorage.getItem('name')){
        this.name = localStorage.getItem('name');
      }

    $('body').removeClass('mat-typography');

  }

  logout(){
    localStorage.removeItem("role")
    localStorage.removeItem("type")
    localStorage.removeItem("token")
    localStorage.removeItem("isAuthenticated");
    this.router.navigate(['admin/login'])
  }

}
