import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';
import { AbstractControl, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';

@Component({
	selector: 'app-subcategories',
	templateUrl: './subcategories.component.html',
	styleUrls: ['./subcategories.component.css']
})
export class SubcategoriesComponent implements OnInit {

	subCategoriesArray: any = [];
	totalsubCategoriesArray: any = [];
	categoryName: any = 1;
	subCategoryName: any = "";
	subCatName: any = "";
	activeId = "";
	updateOrInsert: boolean = false;
	image: any = "assets/images/category_upload_img.png";
	imageSrc: string | ArrayBuffer | any = "assets/images/category_upload_img.png";
	files: any;
	baseurls = environment.adminbaseUrl;
	viewReader: any = [];
	searchText: string = "";
	searchArray: any = [];
	submitted = false;
	uploadedFile: any = "";
	minAge = 0;
	maxAge = 0;
	therapyName: any = "";
	timeline: any = "";
	norecordfound: any = false;
	ageGroup: string = " Years";
	myGroup = new FormGroup({});
	no_of_repetitions: any = "";
	submitdisable: any = false;
	length: any = 10;
	pageSize: any = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	Index: any = 1;
	stype:any = 1;

	constructor(private _fb: FormBuilder, private service: ServiceFileService) { }




	subcategories: any = [
		'Mindful musicians (0-6 Years)',
		'Neurodynamic music therapy',
		'Rooted private lessions'
	]
	defaultSubcategories: any = 'Mindful musicians';



	ngOnInit(): void {
		$('.full_page_loader_app').show();
		$('.dashboard-table-outer').css('opacity', 0).hide();
		this.myGroup = this._fb.group({
			'categoryName': ['', [Validators.required]],
			// 'minAge': ['', [Validators.required]],
			// 'maxAge': ['', [Validators.required]],
			// 'timeline': ['', [Validators.required]],
			// 'therapyName': ['', [Validators.required]],
			'no_of_repetitions': ['', [Validators.required]],
			'subCatName': ['', [Validators.required]],
			'stype': ['']

		});
		this.getSubCategories()
	}


	validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
			const control = myGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}


	// onImageChange(event:any){ 

	//   if (event.target.files && event.target.files[0]){ 
	//       var formData = new FormData();
	//       const file = event.target.files[0]; 
	//       const reader = new FileReader(); 
	//       reader.onload = e => this.imageSrc = reader.result; 
	//       let fileList: FileList = event.target.files;
	//       this.files = fileList[0];
	//       this.formData.append("file", event.target.files[0]);
	//       this.uploadedFile = event.target.files[0].name;
	//       reader.readAsDataURL(file); 
	//       console.log(this.uploadedFile);
	//   } 
	// }


	getSubCategories() {

		let data = {
			searchValue: this.searchText
		}

		this.service.getSubCategories(data).subscribe(
			(response: any) => {
				this.totalsubCategoriesArray = response.message;
				this.subCategoriesArray = response.message.slice(0, 10);
				if (this.subCategoriesArray.length == 0) {
					this.norecordfound = true;
				}else{
					this.norecordfound = false;
		  
				  }
				$('.full_page_loader_app').hide();
				$('.dashboard-table-outer').css('opacity', 1).show();
			},
			err => {
				console.log("err", err)
			}
		);
	}


	saveCategories(e: any) {
		this.validateAllFields(this.myGroup);
		if (this.myGroup.valid) {
			this.submitdisable = true;
			let data;

			if (this.categoryName == 1) {
				// data = {
				// 	1:1,
				// 	min_age : this.minAge,
				// 	max_age: this.maxAge,

				// }
				data = {
					cat_type: 1,
					timeline: this.subCatName,
					no_of_repetitions: this.no_of_repetitions

				}
			}

			if (this.categoryName == 2) {
				data = {
					cat_type: 2,
					therapyName: this.subCatName,
					no_of_repetitions: this.no_of_repetitions,
					stype:this.stype

				}
			}

			if (this.categoryName == 3) {
				data = {
					cat_type: 3,
					subCatName: this.subCatName
				}
			}

			if (!this.updateOrInsert) {
				this.service.addSubCategories(data).subscribe(

					(response: any) => {
						if (response.status == '1') {

							this.getSubCategories()
							var close = document.getElementById('closecategoryModal');
							if (close) {
								close.click()
							}
							$('button.close').trigger('click');



						}

					},
					err => {
						console.log("err", err)
						this.submitdisable = false;


					}
				);
			} else {

				let data;
				if (this.categoryName == 1) {
					// data = {
					// 	cat_type:1,
					// 	min_age : this.minAge,
					// 	max_age: this.maxAge,
					// 	id:this.activeId
					// }
					data = {
						cat_type: 1,
						timeline: this.subCatName,
						id: this.activeId,
						no_of_repetitions: this.no_of_repetitions

					}
				}

				if (this.categoryName == 2) {
					data = {
						cat_type: 2,
						therapyName: this.subCatName,
						no_of_repetitions: this.no_of_repetitions,
						id: this.activeId,
						stype:this.stype
					}
				}
				if (this.categoryName == 3) {
					data = {
						cat_type: 3,
						subCatName: this.subCatName,
						id: this.activeId
					}
				}

				this.service.updateSubCategories(data).subscribe(
					(response: any) => {

						this.getSubCategories()
						this.updateOrInsert = false;

						var close = document.getElementById('edit-category');
						if (close) {
							close.click()
						}

						$('button.close').trigger('click');


					},
					err => {
						console.log("err", err)
						this.submitdisable = false;


					}
				);

			}
		}




	}

	updateData(min_age: any, max_age: any, subcat_name: any, id: any, cat_type: any, no_of_repetitions: any,stype:any) {
		this.stype = stype;
		this.submitdisable = false;
		this.minAge = min_age,
			this.maxAge = max_age,
			this.categoryName = cat_type;
		this.timeline = subcat_name;
		this.therapyName = subcat_name;
		this.subCatName = subcat_name;
		this.updateOrInsert = true;
		this.activeId = id;
		this.no_of_repetitions = no_of_repetitions;
		$('.type').each(function(){
			if($(this).val() == stype){
				$(this).prop('checked','true');
			}
		})


	}

	deleteSubCategory(id: any) {

		let data = {
			id: id
		}
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			confirmButtonColor: '#738c72',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.isConfirmed) {
				this.service.deleteSubCategories(data).subscribe(
					(response: any) => {
						this.getSubCategories()
					},
					err => {
						console.log("err", err)
					}
				);
			}
		});

	}

	searchSubCategories(e: any) {

		let data = {
			searchValue: this.searchText
		}
		this.service.getSubCategories(data).subscribe(
			(response: any) => {
				this.totalsubCategoriesArray = response.message;
				this.subCategoriesArray = response.message.slice(0, 10);

			},
			err => {
				console.log("err", err)
			}
		);
	}

	allowNumeric(e: any) {

		var charCode = (e.which) ? e.which : e.keyCode

		if (String.fromCharCode(charCode).match(/[^0-9]/g)) {

			e.preventDefault();
		}

	}
	addCategorypopup() {
		this.submitdisable = false;
		$('#add-category').find('form').trigger('reset');
		this.categoryName = "1";
		this.timeline = "";
		this.therapyName = "";
		this.subCatName = "";
		this.activeId = "";
		this.stype = 1;
		this.updateOrInsert = false;
		

	}

	selectCategory(event: any) {
		this.categoryName = event.target.value;
		this.subCatName = "";
		if (this.categoryName == 1) {
			this.no_of_repetitions = "";

		} else if(this.categoryName == 2 && this.stype == 1) {
			this.no_of_repetitions = 1;
		}else{
			this.no_of_repetitions = 1;

		}
		this.stype = 1;
		$('.type:eq(0)').prop('checked','true');


	}

	selectType(event:any){
		$('.type').removeAttr('checked');
		$(event.target).prop('checked','true');
		this.stype = event.target.value;
		if (this.stype == 2) {
			this.no_of_repetitions = "";

		} else {
			this.no_of_repetitions = 1;
		}
	}

	OnPageEvent(event: PageEvent) {
		const startIndex = event.pageIndex * event.pageSize;
		let endIndex = startIndex + event.pageSize;
		if (endIndex > this.totalsubCategoriesArray.length) {
			endIndex = this.totalsubCategoriesArray.length;
		}
		this.Index = startIndex + 1;
		this.subCategoriesArray = this.totalsubCategoriesArray.slice(startIndex, endIndex);
	}

}
