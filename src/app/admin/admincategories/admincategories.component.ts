import { Component, OnInit } from '@angular/core';
import { ServiceFileService } from 'src/service-file.service';
import { environment } from 'src/environments/environment';
import { AbstractControl,FormGroup, FormControl,FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-admincategories',
  templateUrl: './admincategories.component.html',
  styleUrls: ['./admincategories.component.css']
})
export class AdmincategoriesComponent implements OnInit {
  categoriesArray: any = [];
  categoryName = "";
  activeId = "";
  categorySession = "";
  categoryprice = "";
  updateOrInsert: boolean = false;
  image:any = "assets/images/category_upload_img.png";
  imageSrc: string | ArrayBuffer |any = "assets/images/category_upload_img.png";
  files: any;
  private formData = new FormData();
  baseurls = environment.adminbaseUrl;
  viewReader:any=[];
  searchText:string="";
  searchArray:any=[];
  submitted = false;

  uploadedFile:any="";



  categoryForm = new FormGroup({
    file: new FormControl('', [Validators.required]),
    categoryName:new FormControl('',[Validators.required]),
    categorySession:new FormControl('',[Validators.required]),
    categoryprice:new FormControl('',[Validators.required])
  });

 
  constructor(private service: ServiceFileService,
              private formBuilder: FormBuilder) { 
               
              }

  ngOnInit(): void {
    this.getCategories()
  }

  get f(): { [key: string]: AbstractControl } {
    return this.categoryForm.controls;
  }

  onImageChange(event:any){ 

    if (event.target.files && event.target.files[0]){ 
        var formData = new FormData();
        const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        let fileList: FileList = event.target.files;
        this.files = fileList[0];
       // this.formData.append("file", event.target.files[0]);
        this.uploadedFile = event.target.files[0].name;
        reader.readAsDataURL(file); 
        console.log(this.uploadedFile);
    } 

    // if (event.target.files.length > 0) {
    //   var formData = new FormData();
    //   const file = event.target.files[0];
    //   this.uploadedFile = file.name;
    //   console.log(this.uploadedFile);
    // }

  }

  saveCategory(){
    this.submitted = true;
    if (this.categoryForm.invalid) {
      return;
    }

    let data = {
      file:this.uploadedFile,
      name:this.categoryForm.value.categoryName,
      categorySession:this.categoryForm.value.categorySession,
      categoryprice:this.categoryForm.value.categoryprice,
    }

    console.log(data);

    if (!this.updateOrInsert){
          this.service.addCategories(data).subscribe(
            (response: any) => {
              this.categoryName = "";
              this.getCategories()
              var close = document.getElementById('closecategoryModal');
              if(close){
                close.click()
              }              
            },
            err => {
              console.log("err", err)
            }
          );
    }
  }

  

  

  




  getCategories() {
    this.service.getCategories().subscribe(
      (response: any) => {
        this.categoriesArray = response.message;
       
      },
      err => {
        console.log("err", err)
      }
    );
  }

  
  // SaveCategories() {
  //   const formdate = new FormData();
  //   formdate.append('categorySession', this.categorySession)
  //   formdate.append('categoryprice', this.categoryprice)
  //   formdate.append('name', this.categoryName)
    
  //   if(this.files){
  //     formdate.append('file', this.files, this.files.name);
  //   }
  //   if (!this.updateOrInsert) {
  //     this.service.addCategories(formdate).subscribe(
  //       (response: any) => {
  //         this.categoryName = "";
  //         this.getCategories()
  //         var close = document.getElementById('closecategoryModal');
  //         if(close){
  //           close.click()
  //         }
          
  //       },
  //       err => {
  //         console.log("err", err)
  //       }
  //     );
  //   } else {
  //     const formdate = new FormData();
  //     formdate.append('categorySession', this.categorySession)
  //     formdate.append('categoryprice', this.categoryprice)
  //     formdate.append('name', this.categoryName)
  //     formdate.append('id', this.activeId)
  //     if(this.files){
  //       formdate.append('file', this.files, this.files.name);
  //     }
      
  //     this.service.updateCategories(formdate).subscribe(
  //       (response: any) => {
  //         this.categoryName = "";
  //         this.updateOrInsert = false;
  //         this.getCategories()
  //         var close = document.getElementById('closecategoryModal');
  //         if(close){
  //           close.click()
  //         }
  //       },
  //       err => {
  //         console.log("err", err)
  //       }
  //     );
  //   }

  // }



  updateData(name: any, id: any,post:any) {
    this.categoryName = name;
    this.categoryprice = post.price;
    this.categorySession = post.session_type;
    this.image= this.baseurls+""+post.image
    this.updateOrInsert = true;
    this.activeId = id;
  }


  // changeStatus(post: any, event: any) {
  //   console.log(event.srcElement.checked)
  //   var status = 0;
  //   if (event.srcElement.checked) {
  //     if (confirm('Are you sure you want to Active this category')) {
       
  //       status = 1;
  //     } else {
  //       event.srcElement.checked = !event.srcElement.checked
  //     }
  //   }
  //   else{
  //     if (confirm('Are you sure you want to inctive this category')) {
  //       status = 0;
  //     } else {
  //       event.srcElement.checked = !event.srcElement.checked
  //     }
  //   }
  //   var daa = {
  //     id:post.id,
  //     status:status
  //   }

  //   this.service.updateCategoriesStatus(daa).subscribe(
  //     (response: any) => {
  //       this.getCategories()
  //       alert("Status changed")
  //     },
  //     err => {
  //       console.log("err", err)
  //     }
  //   );
  // }


  // deleteCategories(id:any){
  //   if (confirm('Are you sure you want to delete this category')) {
  //     this.service.deleteCategories({id:id}).subscribe(
  //       (response: any) => {
  //         this.getCategories()
  //         alert("Category deleted")
  //       },
  //       err => {
  //         console.log("err", err)
  //       }
  //     );
  //   } 
  // }

  
  openViewModal(post:any){
    this.viewReader = post
  }



  searchCategories(e:any){

    let data = {
      searchValue : this.searchText
    }
    console.log(data);
    this.service.searchCategories(data).subscribe(
      (response: any) => {
        this.categoriesArray = response.message;
        console.log(" this.categoriesArray",  this.categoriesArray)
      },
      err => {
        console.log("err", err)
      }
    );
  }


}
