import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceFileService } from 'src/service-file.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import * as $ from 'jquery';
import Swal from 'sweetalert2';
import Inputmask from "inputmask";
import { environment } from 'src/environments/environment';


@Component({
	selector: 'app-donation',
	templateUrl: './donation.component.html',
	styleUrls: ['./donation.component.css']
})
export class DonationComponent implements OnInit {

	stripe: any;
	elements: any;
	card: any = [];
	cdate = new Date();
	noofbookingBool = false;
	activePost: any = [];
	totatcartprice: any = "";
	formProcess: any = '';
	customStripeForm: any;
	submitted: any;
	submitdisable:any = false;
	//error: string="";
	bookingCount: any = 0
	number: any = ""
	expiry_month: any = ""
	expiry_year: any = ""
	cvc: any = "";
	param1: any = "";
	Booking_cartArray: any = [];
	uname: any = "";
	cardHolderName: any = "";
	cardNumber: any = "";
	expiryDate: any = "";
	expiryDateError: any = "";
	expiryMonth: any = "";
	expiryYear: any = "";
	cvvNumber: any = "";
	securityCode: any = "";
	price: any = "";
	priceError: any = "";
	myGroup = new FormGroup({});
	error: boolean = false;
	mainurl:any = environment.PROJECT_URL;
   	backendurl: any = this.mainurl+"/centrallyRooted/public/";
  	STRIPE_PUBLIC_KEY: any = environment.STRIPE_PUBLIC_KEY;
	constructor(private _fb: FormBuilder, private router: Router, private service: ServiceFileService, private route: ActivatedRoute) { }

	ngOnInit(): void {

		this.myGroup = this._fb.group({
			'cardHolderName': ['', [Validators.required]],
			'cardNumber': ['', [Validators.required, Validators.minLength(17), Validators.maxLength(19)]],
			'expiryDate': ['', [Validators.required, Validators.minLength(7)]],
			'cvvNumber': ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(3)]],
			'price': ['', [Validators.required]],
			'uname': ['']
		})


		this.route.queryParams.subscribe(params => {
			this.param1 = params['cat'];
			// this.param2 = params['param2'];

		});


		this.scriptforall();



		this.loadStripe();
		

	}


	make_payment(e: any) {
		var $this = this;
		const amount = this.totatcartprice;

		$(e.target).closest('form').find('div.rerror').show();
		$this.validateAllFields(this.myGroup);
		$('.stripe_error').removeClass('alert alert-danger').text('');
		$this.submitdisable = true;

		setTimeout(() => {


			if ($(e.target).closest('form').find('div').hasClass('alert')) {

				$this.submitdisable = false;

			} else {
				$('.full_page_loader_app').show().addClass('pay');

				(<any>window).Stripe.setPublishableKey($this.STRIPE_PUBLIC_KEY);
				(<any>window).Stripe.createToken({
					number: $this.cardNumber,
					cvc: $this.cvvNumber,
					exp_month: $this.expiryDate.split('/')[0],
					exp_year: $this.expiryDate.split('/')[1],
					name: $this.cardHolderName
				}, stripeResponseHandler);


				function stripeResponseHandler(status: any, response: any) {
					if (response.error) {
						$('.full_page_loader_app').hide().removeClass('pay');
						$this.submitdisable = false;

						$('.stripe_error').addClass('alert alert-danger').text(response.error.message);
					} else {
						$('.stripe_error').removeClass('alert alert-danger').text('');

						// token contains id, last4, and card type
						var token = response['id'];
						var email = response['card']['name'];
						var card = response['card'];
						let unknownname = $('.unknownname:checked').val();

						var name = (unknownname ? unknownname : $this.uname);



						$this.submitPayment(token, name, card);




					}
				}
			}

		}, 1000);

	}

	loadStripe() {
		const $this = this;

		const cyear = $this.cdate.getFullYear();

		const cmonth = $this.cdate.getUTCMonth();
		if (!window.document.getElementById('stripe-scriptjs')) {
			const script = window.document.createElement('script');
			script.id = "stripe-scriptjs";
			script.type = "text/javascript";
			script.src = "https://js.stripe.com/v2/";

			window.document.body.appendChild(script);
		}

		$(document).on('keyup', '.price', function () {
			$this.priceError = false;
			let price = $(this).val();
			var validatePrice = function (price: any) {
				return /^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(price);
			}
			if (validatePrice(price) == false) {
				$this.priceError = true;
			}
		});


		var expiryMask = function (event: any) {
			var inputChar = String.fromCharCode(event.keyCode);
			var code = event.keyCode;
			var allowedKeys = [8];
			if (allowedKeys.indexOf(code) !== -1) {
				return;
			}

			event.target.value = event.target.value.replace(
				/^([1-9]\/|[2-9])$/g, '0$1/'
			).replace(
				/^(0[1-9]|1[0-2])$/g, '$1/'
			).replace(
				/^([0-1])([3-9])$/g, '0$1/$2'
			).replace(
				/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2'
			).replace(
				/^([0]+)\/|[0]+$/g, '0'
			).replace(
				/[^\d\/]|^[\/]*$/g, ''
			).replace(
				/\/\//g, '/'
			);
		}

		var splitDate = function ($domobj: any, value: any) {
			var regExp = /(1[0-2]|0[1-9]|\d)\/(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)/;
			var matches = regExp.exec(value);
			if (matches) {
				$domobj.siblings('input[name$="expiryMonth"]').val(matches[1]);
				$domobj.siblings('input[name$="expiryYear"]').val(matches[2]);

			}
		}


		$(document).on("focusout", ".card_exp", function () {
			splitDate($(this), $(this).val());
		});


		$(document).on("keydown", ".card_exp", function () {
			let val = this.value;
			if ($(this).val() == "") {
				$this.expiryDateError = false;

			}

		});
		$(document).on("keyup", ".card_exp", function (event) {
			$this.expiryDateError = false;
			expiryMask(event);

			if ($(this).val()) {
				let val = this.value;


				$this.expiryMonth = val.split('/')[0];
				$this.expiryYear = val.split('/')[1];
				if ($this.expiryMonth < 10 && $this.expiryMonth.toString().length < 2) {
					$this.expiryDateError = true;
				}
				if ('' === $this.expiryMonth || isNaN($this.expiryMonth) || $this.expiryMonth < 1 || $this.expiryMonth > 12) {
					$this.expiryDateError = true;
				}


				if ('' === $this.expiryYear || isNaN($this.expiryYear) || $this.expiryYear.length != 4 || $this.expiryYear < cyear) {
					$this.expiryDateError = true;

				}

				if ($this.expiryYear <= cyear) {
					if ($this.expiryMonth <= cmonth) {
						$this.expiryDateError = true;
					}
				}


			}

		});

	}


	submitPayment(stripeToken: any, name: any, card:any) {
		const $this = this;




		const data = {
			stripeToken: stripeToken,
			name: name,
			total_ammount_paid: this.price,
			card: card

			// class:this.activePost,
		}
		this.service.donation(data).subscribe(
			(response: any) => {
				$('.full_page_loader_app').hide();
				if (response.error == 1) {
					$this.submitdisable = false;

					$('.stripe_error').addClass('alert alert-danger').text(response.message);
				} else {
					$('.modal.show .close').trigger('click');
					Swal.fire({
						title: "Donation Successfully",
						text: "Thank You",
						icon: "success"
					}).then(function () {

						$('#donationnow form').trigger('reset');
						$this.submitdisable = false;




					});
				}



			},
			err => {
				$this.submitdisable = false;

				$('.full_page_loader_app').hide();

				console.log("err", err)

			}
		);


	}

	validateAllFields(myGroup: FormGroup) {
		Object.keys(myGroup.controls).forEach(field => {
			const control = myGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}





	scriptforall() {
		const $this = this;

		$(window).click(function (event: any) {
			setTimeout(() => {


				var modal = document.getElementById("donationnow");
				if (event.target == modal) {
					$('#donationnow form').trigger("reset");

					$('.stripe_error').removeClass('alert alert-danger').text('');

				}

				var modal2 = document.getElementById("add-payment");
				if (event.target == modal2) {
					$('#add-payment form').trigger("reset");

					$('.stripe_error').removeClass('alert alert-danger').text('');

				}
			}, 1000);

		});

		$('.close,.close-btn').click(function (event: any) {
			let cthis = $(this);
			setTimeout(() => {

				cthis.closest('#donationnow').find('form').trigger("reset");

				$('.stripe_error').removeClass('alert alert-danger').text('');

			}, 1000);

		});

		Inputmask({placeholder: "",showMaskOnHover: false, // will do nothing, mask will be shown on hover
		showMaskOnFocus: false}).mask(document.querySelectorAll(".inputmask"));
	setInterval(function(){
	
	  Inputmask({placeholder: "",showMaskOnHover: false, // will do nothing, mask will be shown on hover
	  showMaskOnFocus: false}).mask(document.querySelectorAll(".inputmask"));
	},3000);

	}

	numberOnly(event: any): boolean {
		const charCode = (event.which) ? event.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}

		return true;

	}


	formatCardNumber(event: any) {
		const element = event.target;
		const $this = this;
		setTimeout(() => {
			if (element.value.length > 17) {
				var position = element.selectionStart;
				element.value = element.value.replace(/\W/gi, '').replace(/^(.{4})(.{4})(.{4})(.*)$/, "$1 $2 $3 $4");
				if (element.value.length != 19) {
					element.setSelectionRange(position, position);
				}
			}
			else {
				element.value = element.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
			}
			element.value = element.value.slice(0, 19);
			$this.cardNumber = element.value.trim();
		}, 10);

	}


	allowNumeric(e: any) {

		var charCode = (e.which) ? e.which : e.keyCode

		if (String.fromCharCode(charCode).match(/[^0-9]/g)) {

			e.preventDefault();
		}

	}

	allowAlphabetic(e: any) {
		var regex = new RegExp(/^[a-zA-Z\s]+$/);
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}
		else {
			e.preventDefault();
			return false;
		}
	}


}
