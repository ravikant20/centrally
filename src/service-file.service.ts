import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ServiceFileService {
  rootUrl: string = environment.baseUrl;
  constructor(private http: HttpClient, private router: Router) { }

  getCategories() {
    return this.http.post(this.rootUrl + 'getCategories', null);
  }
  addCategories(data:any) {
    return this.http.post(this.rootUrl + 'addCategories', data);
  }
  updateCategories(data:any){
    return this.http.post(this.rootUrl + 'updateCategories', data);
  }
  updateCategoriesStatus(data:any){
    return this.http.post(this.rootUrl + 'updateCategoriesStatus', data);
  }
  deleteCategories(data:any){
    return this.http.post(this.rootUrl + 'deleteCategories', data);
  }
  getTeachers(){
    return this.http.post(this.rootUrl + 'getTeachers', null);
  }
  
  addTeachers(data:any) {
    return this.http.post(this.rootUrl + 'addTeacher', data);
  }
  updateTeachers(data:any){
    return this.http.post(this.rootUrl + 'updateTeachers', data);
  }
  updateTeachersStatus(data:any){
    return this.http.post(this.rootUrl + 'updateTeachersStatus', data);
  }
  deleteTeachers(data:any){
    return this.http.post(this.rootUrl + 'deleteTeachers', data);
  }
  getClasses(){
    return this.http.post(this.rootUrl + 'getClasses', null);
  }

  getClassesByToday(){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getClassesByToday', null, options);
  }

  addClass(data:any){
    return this.http.post(this.rootUrl + 'addClass', data);
  }
  updateClass(data:any){
    return this.http.post(this.rootUrl + 'updateClass', data);
  }

  checkClass(data:any){
    return this.http.post(this.rootUrl + 'checkClass', data);

  }

  getBookings(){
    return this.http.post(this.rootUrl + 'getBookings', null);
  }
  updateBookings(data:any){
    return this.http.post(this.rootUrl + 'updateBookings', data);
  }
  login(data:any){
    return this.http.post(this.rootUrl + 'login', data);
  }
  
  register(data:any){
    return this.http.post(this.rootUrl + 'register', data);
  }

  makePayment(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'checkPayment', data,options);
  }

  zeromakePayment(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'zeromakePayment', data,options);
  }


  myBookings(){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'myBookings', null,options);
  }  





  getUserData(){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getUserData', null,options);
  }
  
  updateUserData(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'updateUserData', data,options);
  }
  changePassword(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'changePassword', data,options);
  }

  forgotPassword(data:any){
    return this.http.post(this.rootUrl + 'forgotPassword', data);
  }
  resetPassword(data:any){
    
    return this.http.post(this.rootUrl + 'resetPassword', data);
  }

  contactUs(data:any){ 
    return this.http.post(this.rootUrl + 'contactUs', data);
  }
  
  /*.......Admin part..............*/
  addTutors(data:any){
    return this.http.post(this.rootUrl + 'addTutors', data);
  }


  getRegisteredUsers(){
    return this.http.post(this.rootUrl + 'getRegisteredUsers', null);
  }

  getTotalClassesDaily(){
    return this.http.post(this.rootUrl + 'getDailyClassesCount', null);
  }

  getTotalNumberOfTeachers(){
    return this.http.post(this.rootUrl + 'getTotalNumberOfTeachers', null);
  }

  getRecentUsersList(){
    return this.http.post(this.rootUrl + 'getRecentUsersList', null);
  }

  getRecentTeachersList(){
    return this.http.post(this.rootUrl + 'getRecentTeachersList', null);
  }

  getRecentBookings(){
    return this.http.post(this.rootUrl + 'getRecentBookings',null);
  }


  getClassesDates(){
    return this.http.post(this.rootUrl + 'getClassesDates',null);
  }

  getTodaysSessions(data:any){
    return this.http.post(this.rootUrl + 'getTodaysSessions',data);
  }

  getMusic(){
    return this.http.post(this.rootUrl + 'getMusic',null);
  }

  uploadMusic(data:any){
    return this.http.post(this.rootUrl + 'uploadMusic',data);
  }

  removeMusic(data:any){
    return this.http.post(this.rootUrl + 'removeMusic',data);
  }

  searchClasses(data:any){
    return this.http.post(this.rootUrl + 'searchClasses',data);
  }

  searchCategories(data:any){
    return this.http.post(this.rootUrl + 'searchCategories',data);
  }

  searchTeachers(data:any){
    return this.http.post(this.rootUrl + 'searchTeachers',data);
  }

  searchUsers(data:any){
    return this.http.post(this.rootUrl + 'searchUsers',data);
  }

  getRecordsWithCategories(data:any){
    return this.http.post(this.rootUrl + 'getRecordsWithCategories',data);
  }

  getSlotBycategory(data:any){
    return this.http.post(this.rootUrl + 'getSlotBycategory',data);
  }

  getAllUsersLists(){
    return this.http.post(this.rootUrl + 'getAllUsersLists',null);
  }

  addSubCategories(data:any){
    return this.http.post(this.rootUrl + 'addSubCategories',data);
  }

  getSubCategories(data?:any){
    return this.http.post(this.rootUrl + 'getSubCategories',data);
  }

  updateSubCategories(data:any){
    return this.http.post(this.rootUrl + 'updateSubCategories',data);
  }

  deleteSubCategories(data:any){
    return this.http.post(this.rootUrl + 'deleteSubCategories',data);
  }

  
  searchSubCategories(data:any){
    return this.http.post(this.rootUrl + 'searchSubCategories',data);
  }

  addNumberofSiblings(data:any){
    return this.http.post(this.rootUrl + 'addNumberofSiblings', data);
  }

  addSiblings(data:any){
    return this.http.post(this.rootUrl + 'addSiblings', data);
  }

  addCardDetails(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'savecard', data,options);
  }

  subcatListing() {
    return this.http.post(this.rootUrl + 'subcatListing', null);  
  } 

  getSubCategoriesbyid(data:any){
    return this.http.post(this.rootUrl + 'getSubCategoriesbyid', data);  

  }

  saveBookingcart(data:any){
   
    return this.http.get(this.rootUrl + 'saveBookingcart?'+data);  
  }

  getBookingcart(){

    let headers = new HttpHeaders({ //let is used to create temporary
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getBookingcart', null,options);
  }

  removeBookingcart(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'removeBookingcart', data,options);
  }



  donation(data:any){
 
    return this.http.post(this.rootUrl + 'donation', data);
  }

  getcardDetails(){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getcardDetails', null,options);
  }

  getcardDetailsByid(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getcardDetailsByid', data,options);
  }

  removecardDetails(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'removecardDetails', data,options);
  }

  
  deleteClass(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'deleteClass', data,options);
  }


  getClassesByUser(){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getClassesByUser', null,options);
  }
  

  resetPasswordlink(data:any){
    return this.http.post(this.rootUrl + 'resetPasswordlink', data);
  }

  inquiry_send(data:any){
    return this.http.post(this.rootUrl + 'inquiry_send', data);

  }

  downloadMusic(data:any){
    return this.http.post(this.rootUrl + 'downloadMusic', data,{responseType: 'blob'});
  }

  
  getBookingUserSubscription(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'getBookingUserSubscription', data,options);
  }

  getSubCategoriesbyidn(data:any){
    return this.http.post(this.rootUrl + 'getSubCategoriesbyidn', data);

  }
  
  cancelSubscription(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'cancelSubscription', data,options);
  }

  pauseplaySubscription(data:any){
    let headers = new HttpHeaders({ //let is used to create temporary
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    });
    let options = { headers: headers };
    return this.http.post(this.rootUrl + 'pauseplaySubscription', data,options);
  }

  getDonationdata(){
    return this.http.post(this.rootUrl + 'getDonationdata', null);
  }
  checkuserlimitforclass(data:any){
    return this.http.post(this.rootUrl + 'checkuserlimitforclass', data);
  }


  searchBookings(data:any){
    return this.http.post(this.rootUrl + 'searchBookings',data);
  }
  updateBookingClass(data:any){
    return this.http.post(this.rootUrl + 'updateBookingClass', data);
  }
}