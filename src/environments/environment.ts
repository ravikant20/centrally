// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
/*************live url**************/
// const mainurl = window.location.origin;
/*************stage url**************/
const mainurl = "http://54.176.220.165";
export const environment = {
  production: false,
  PROJECT_URL: mainurl,
  baseUrl: mainurl + "/centrallyRooted/public/api/",
  adminbaseUrl: mainurl + "/centrallyRooted/public/images/",
  uploadsImageUrl: mainurl + "/centrallyRooted/public/uploads/",
  /*************live stripe public key**************/
  // STRIPE_PUBLIC_KEY : "pk_live_51JQC3AAa1AD2aKZg9PGZEwPzUuiHuc3qWGYR2NzZ8nb8BSUljwK1WnlQiTA9Bev9BV5Glw1RoJBe2ShLywnMy5fq00yZKJbbOO"
  /*************stage stripe public key**************/
  STRIPE_PUBLIC_KEY: "pk_test_51JPib0SIyJRfppK0fV5TxfwnyUH2qTKFpTOx6L5kJQWE2o0Z8zGBlKJFJUgyqxGLW8QNXug3IpCfjq8Cw8QJl5Ye00gBdjCZf0"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
